
# Urchin bot

**NOTICE:** This project is deprecated and abandoned.

My personal bot to handle various chat clients (currently only Discord, but more chat APIs are planned).

Heavy work-in-progress, many things still need to be implemented, but it's now up and running.  
Not recommended for use until a remote administrative console is created and some configuration
aspects are delegated to a database instead of a JSON file.

Licensed under the MS-PL.
