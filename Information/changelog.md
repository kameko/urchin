
From ExtBot (2017) to Urchin v0.1 (2021):
 - Messages are now instantly added to a fresh Markov chain
 - Markov chain is refreshed every hour instead of every day
 - Markov chain no longer lowercases all text, it keeps casing in-tact while still operating as if casing didn't exist
 - Due to the above enhancement, the Markov chain responder will now post links (it intentionally didn't before because of a bug)
 - Markov chain responder uses a strict whitelist, will not reply in channels not in it's whitelist
 - Thanks to the fresh new codebase, I'll be adding lots of new features and AI in the future
 - Automatically saves any and all attachments posted by anyone
 - Extensively logs all messages
 - Watches everything you do
