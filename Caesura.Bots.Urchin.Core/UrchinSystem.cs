
namespace Caesura.Bots.Urchin.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Common.Configurations;
    using Common.Infrastructure;
    using Endpoints;
    using Services;
    
    public sealed class UrchinSystem : IAsyncDisposable
    {
        public ServiceContainer Services => services;
        public Func<CancellationToken, Task> OnStart;
        public Func<Task> OnShutdown;
        
        private ServiceContainer services;
        private List<IRemoteClient> clients;
        private ILogger log;
        private ExceptionListener listener;
        private CancellationTokenSource cts;
        private bool running;
        
        public UrchinSystem(
            ILoggerFactory logger,
            UrchinConfigurationRoot config,
            ExceptionListener exceptionListener,
            CancellationTokenSource tokenSource
        )
        {
            log        = logger.CreateLogger<UrchinSystem>();
            listener   = exceptionListener;
            cts        = tokenSource;
            services   = new(logger, config, listener);
            clients    = new();
            
            OnStart    += delegate { return Task.CompletedTask; };
            OnShutdown += delegate { return Task.CompletedTask; };
            
            running = false;
        }
        
        public void AddClient(IRemoteClient client)
        {
            if (running)
            {
                throw new InvalidOperationException("Cannot add clients after system is running.");
            }
            
            clients.Add(client);
            client.OnRequestShutdown += OnClientRequestShutdown;
        }
        
        public async Task Start()
        {
            log.LogInformation("Starting services...");
            
            if (running)
            {
                throw new InvalidOperationException("Attemping to start the application twice.");
            }
            running = true;
            
            var success = false;
            try
            {
                var token = cts.Token;
                await OnStart.Invoke(token);
                await services.Start(token);
                foreach (var client in clients)
                {
                    await client.AutoStart(token);
                }
                success = true;
            }
            catch (Exception e)
            {
                listener.Tell(e);
                log.LogError("System did not start successfully.");
            }
            
            if (success)
            {
                log.LogInformation("System started successfully.");
            }
        }
        
        public async ValueTask DisposeAsync()
        {
            try
            {
                await services.DisposeAsync();
                foreach (var client in clients)
                {
                    await client.DisposeAsync();
                }
                
                await OnShutdown.Invoke();
            }
            catch (Exception e)
            {
                log.LogError(e, "Error disposing of system.");
                listener.Tell(e);
            }
        }
        
        private void OnClientRequestShutdown(IRemoteClient client, string reason)
        {
            log.LogCritical("Shutdown from {client}. Reason: {reason}", client.Name, reason);
            cts.Cancel();
        }
    }
}
