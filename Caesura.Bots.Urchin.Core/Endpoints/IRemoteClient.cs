
namespace Caesura.Bots.Urchin.Core.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface IRemoteClient : IAsyncDisposable
    {
        string Name { get; }
        bool Running { get; }
        event Action<IRemoteClient, string>? OnRequestShutdown;
        
        Task Start(CancellationToken token);
        Task AutoStart(CancellationToken token);
    }
}
