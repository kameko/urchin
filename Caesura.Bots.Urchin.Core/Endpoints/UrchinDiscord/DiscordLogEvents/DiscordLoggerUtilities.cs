
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using Common.Extensions;
    
    public static class DiscordLoggerUtilities
    {
        public static async Task FormatGuildChannelAuthorAttachments(
            DiscordClient client,
            StringBuilder s,
            List<object> o,
            int padlen,
            string sepstr,
            DiscordMessage message,
            DiscordChannel channel,
            DiscordGuild? guild
        )
        {
            if (!channel.IsPrivate)
            {
                s.AppendLine();
                s.Append("Guild".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {guild_name} ({guild_id})");
                o.Add(guild?.Name ?? "URCHIN: GUILD NULL");
                o.Add(guild?.Id ?? 0);
                
                s.AppendLine();
                s.Append("Channel".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {channel_name} ({channel_id})");
                o.Add(channel.Name);
                o.Add(channel.Id);
            }
            else
            {
                s.AppendLine();
                s.Append("Channel".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" Direct Message ({channel_id})");
                o.Add(channel.Id);
            }
            
            if (message.Author is not null)
            {
                s.AppendLine();
                s.Append("Author".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {author_name}");
                o.Add(GetUsername(message.Author));
                
                var nickname_error = false;
                if (guild is not null)
                {
                    try
                    {
                        var member = await guild.GetMemberAsync(message.Author.Id);
                        if (!string.IsNullOrEmpty(member.Nickname))
                        {
                            s.Append(" \"{author_nickname}\"");
                            o.Add(member.Nickname);
                        }
                    }
                    catch
                    {
                        nickname_error = true;
                    }
                }
                
                s.Append(" ({author_id})");
                o.Add(message.Author.Id);
                
                if (nickname_error)
                {
                    s.Append(" (Server error when attempting to fetch nickname)");
                }
            }
            
            if (message.Attachments.Count > 0)
            {
                var attach_count = 0;
                foreach (var attachment in message.Attachments)
                {
                    s.AppendLine();
                    s.Append("Attachment".PadRight(padlen));
                    s.Append(sepstr);
                    s.Append(" {attachment_url_");
                    s.Append(attach_count);
                    s.Append("}");
                    o.Add(attachment.Url);
                    
                    attach_count++;
                }
            }
        }
        
        public static async Task ReactionsToString(
            DiscordClient client,
            StringBuilder s,
            List<object> o,
            int padlen,
            string sepstr,
            DiscordMessage message
        )
        {
            var get_message_error = false;
            var get_users_error = false;
            
            var reactions = message.Reactions;
            try
            {
                // Not sure if this is required, but, why not, this isn't
                var channel     = await client.GetChannelAsync(message.ChannelId);
                var new_message = await channel.GetMessageAsync(message.Id);
                reactions       = new_message.Reactions;
            }
            catch
            {
                get_message_error = true;
            }
            
            s.AppendLine();
            s.Append("Reactions".PadRight(padlen));
            s.Append(sepstr);
            s.Append(" {reaction_count}");
            o.Add(reactions.Count);
            
            var emoji_count = 0;
            foreach (var reaction in reactions)
            {
                var emoji_name = reaction.Emoji.GetDiscordName();
                
                s.AppendLine();
                s.Append(" --> {emoji_name_");
                s.Append(emoji_count);
                s.Append("}");
                o.Add(emoji_name);
                
                if (reaction.Emoji.Id > 0)
                {
                    s.Append(" ({emoji_id_");
                    s.Append(emoji_count);
                    s.Append("})");
                    o.Add(reaction.Emoji.Id);
                }
                
                if (reaction.Count > 1)
                {
                    s.Append(" x{emoji_reaction_count_");
                    s.Append(emoji_count);
                    s.Append("}");
                    o.Add(reaction.Count);
                }
                
                try
                {
                    var users = await message.GetReactionsAsync(reaction.Emoji);
                    
                    var nicks = new Dictionary<ulong, string>(users.Count);
                    if (!message.Channel.IsPrivate && message.Channel.Guild is not null)
                    {
                        foreach (var user in users)
                        {
                            try
                            {
                                var member = await message.Channel.Guild.GetMemberAsync(user.Id);
                                if (!string.IsNullOrEmpty(member.Nickname))
                                {
                                    nicks.Add(user.Id, member.Nickname);
                                }
                            }
                            catch
                            {
                                // ignore.
                            }
                        }
                    }
                    
                    s.AppendLine();
                    s.Append(" Added by: ");
                    
                    var reactor_count = 0;
                    foreach (var user in users)
                    {
                        s.AppendLine();
                        s.Append("   --> {emoji_");
                        s.Append(emoji_count);
                        s.Append("_reactor_user_name_");
                        s.Append(reactor_count);
                        s.Append("}");
                        o.Add(GetUsername(user));
                        
                        if (nicks.TryGetValue(user.Id, out var nick))
                        {
                            s.Append(" \"{emoji_");
                            s.Append(emoji_count);
                            s.Append("_reactor_user_nickname_");
                            s.Append(reactor_count);
                            s.Append("}\"");
                            o.Add(nick);
                        }
                        
                        s.Append(" ({emoji_");
                        s.Append(emoji_count);
                        s.Append("_reactor_user_id_");
                        s.Append(reactor_count);
                        s.Append("})");
                        o.Add(user.Id);
                        
                        reactor_count++;
                    }
                }
                catch
                {
                    get_users_error = true;
                }
                
                emoji_count++;
            }
            
            if (get_message_error)
            {
                s.AppendLine();
                s.Append(" (Server error when attempting to get current version of message)");
            }
            if (get_users_error)
            {
                s.AppendLine();
                s.Append(" (Server error when attempting to get users)");
            }
        }
        
        public static string GetUsername(this DiscordUser? user)
        {
            if (user is not null && !string.IsNullOrEmpty(user.Username))
            {
                return $"{user.Username}#{user.Discriminator}".RemoveControlChar();
            }
            else
            {
                return "[INVALID USER NAME]";
            }
        }
    }
}
