
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text.RegularExpressions;
    
    internal class DiscordLoggerEasterEggs
    {
        // Not working with Windows Terminal, ♂ just prints a newline :( 
        // Will re-enable if it ever gets fixed.
        internal const bool enable_get_separator = false;
        
        private static string[] gachis = new[]
        {
            "fuck you",
            "no fuck you leatherman",
            "oh shit",
            "im sorry",
            "oh shit im sorry",
            "sorry for what",
            "our daddy taught us not to be ashamed of our dicks",
            "sorry for what our daddy taught us not to be ashamed of our dicks",
            "slaves get your ass back here",
            "you got me mad now",
            "yugamineena",
        };
        
        internal static string GetSeparator(string content, string default_separator)
        {
            #pragma warning disable CS0162 // Unreachable code.
            
            if (!enable_get_separator)
            {
                return default_separator;
            }
            
            content = Regex.Replace(content.ToLower().Trim(), @"[^\w\s]", "");
            
            for (var i = 0; i < gachis.Length; i++)
            {
                if (content == gachis[i])
                {
                    return "♂";
                }
            }
            
            return default_separator;
            
            #pragma warning restore
        }
    }
}
