
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    
    public sealed class SocketEventHandlers
    {
        private ILogger log;
        
        public SocketEventHandlers(ILogger logger)
        {
            log = logger;
        }
        
        public Task HandleWebhooksUpdated(DiscordClient client, WebhooksUpdateEventArgs args)
        {
            // Unimplemented.
            return Task.CompletedTask;
        }
        
        public Task HandleOpened(DiscordClient client, SocketEventArgs args)
        {
            // log.LogInformation("Discord socket opened");
            return Task.CompletedTask;
        }
        
        public Task HandleClosed(DiscordClient client, SocketCloseEventArgs args)
        {
            // log.LogInformation("Discord socket closed ({code}): {message}", args.CloseCode, args.CloseMessage);
            return Task.CompletedTask;
        }
        
        public Task HandleErrored(DiscordClient client, SocketErrorEventArgs args)
        {
            log.LogInformation(
                "Discord socket errored:" + Environment.NewLine +
                " --> {exception_message}",
                args.Exception.Message
            );
            return Task.CompletedTask;
        }
    }
}
