
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Extensions;
    
    internal class MessageEventHandlers
    {
        private ILogger log;
        private string sepstr;
        
        private ConcurrentDictionary<ulong, ulong> lastMessage;
    
        public MessageEventHandlers(ILogger logger, string separator)
        {
            log         = logger;
            sepstr      = separator;
            lastMessage = new();
        }
        
        public Task HandleSending(DiscordClient client, DiscordChannel channel, DiscordMessageBuilder message)
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            
            var padlen = (message.Files.Count > 0
                          ? "Attachment".Count()
                          : "Content".Count()) + 1;
            
            if (channel.IsPrivate)
            {
                s.Append("Sending Discord Direct Message:");
            }
            else
            {
                s.Append("Sending Discord Message:");
            }
            
            if (!channel.IsPrivate && channel.Guild is not null)
            {
                s.AppendLine();
                s.Append("Guild".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {guild_name} ({guild_id})");
                o.Add(channel.Guild.Name);
                o.Add(channel.Guild.Id);
            }
            
            if (channel.IsPrivate)
            {
                s.AppendLine();
                s.Append("Channel".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" Direct Message ({channel_id})");
                o.Add(channel.Id);
            }
            else
            {
                s.AppendLine();
                s.Append("Channel".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {channel_name} ({channel_id})");
                o.Add(channel.Name);
                o.Add(channel.Id);
            }
            
            if (message.Files.Count > 0)
            {
                s.AppendLine();
                s.Append("Attachment".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {attachment_filename}");
                o.Add(message.Files.First().FileName);
            }
            
            s.AppendLine();
            s.Append("Content".PadRight(padlen));
            s.Append(sepstr);
            s.Append(" {content}");
            
            o.Add(message.Content);
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            return Task.CompletedTask;
        }
        
        public Task HandleAcknowledged(DiscordClient client, MessageAcknowledgeEventArgs args)
        {
            log.LogInformation("Message acknowledged: {message_id}", args.Message.Id);
            return Task.CompletedTask;
        }
        
        public async Task HandleCreated(DiscordClient client, MessageCreateEventArgs args)
        {
            lastMessage[args.Channel.Id] = args.Message.Id;
            
            if (args.Author == client.CurrentUser)
            {
                return;
            }
            
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            
            var padlen = (args.Message.Reactions.Count > 0
                          ? "Reactions".Count()
                          : "Content".Count()) + 1;
            if (args.Message.Attachments.Count > 0)
            {
                padlen = "Attachment".Count() + 1;
            }
            
            if (args.Channel.IsPrivate)
            {
                s.Append("Discord Direct Message Created ");
            }
            else
            {
                s.Append("Discord Message Created ");
            }
            
            s.Append("({message_id}):");
            o.Add(args.Message.Id);
            
            await DiscordLoggerUtilities.FormatGuildChannelAuthorAttachments(
                client, s, o, padlen, sepstr, args.Message, args.Channel, args.Guild
            );
            
            var note1 = false;
            if (string.IsNullOrWhiteSpace(args.Message.Content))
            {
                note1 = true;
            }
            else
            {
                var sep = DiscordLoggerEasterEggs.GetSeparator(args.Message.Content, sepstr);
                
                s.AppendLine();
                s.Append("Content".PadRight(padlen));
                s.Append(sep);
                s.Append(" {content}");
                
                o.Add(args.Message.Content);
            }
            
            if (args.Message.Reactions.Count > 0)
            {
                await DiscordLoggerUtilities.ReactionsToString(client, s, o, padlen, sepstr, args.Message);
            }
            
            if (note1)
            {
                Append("Notice", "notice1", "Message content is empty.");
            }
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            void Append(string name, string tag, object item)
            {
                s!.AppendLine();
                s.Append(name.PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {");
                s.Append(tag);
                s.Append("}");
                
                o!.Add(item);
            }
        }
        
        public async Task HandleUpdated(DiscordClient client, MessageUpdateEventArgs args)
        {
            // Handle the API turning the link into an embed.
            // This is a redundant log message that should be ignored.
            var possibleMessageDup = false;
            if (
                args.MessageBefore.Embeds.Count != args.Message.Embeds.Count &&
                args.Message.Content.Contains("http")
            )
            {
                lastMessage.TryGetValue(args.Channel.Id, out var last_message_id);
                // Even if TryGetValue failed, it's just giving us a ulong, so it's 0, anyway.
                if (
                    last_message_id == args.Message.Id &&
                    args.MessageBefore.Content == args.Message.Content
                )
                {
                    possibleMessageDup = true;
                }
            }
            
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            
            var padlen = "Old Content".Count() + 1;
            
            if (args.Channel.IsPrivate)
            {
                s.Append("Discord Direct Message Updated ");
            }
            else
            {
                s.Append("Discord Message Updated ");
            }
            
            s.Append("({message_id}):");
            o.Add(args.Message.Id);
            
            await DiscordLoggerUtilities.FormatGuildChannelAuthorAttachments(
                client, s, o, padlen, sepstr, args.Message, args.Channel, args.Guild
            );
            
            var note1 = false;
            var note2 = false;
            var note3 = false;
            var before = await GetBefore(args);
            if (before is null || before.Content == args.Message.Content)
            {
                note1 = true;
            }
            else if (string.IsNullOrWhiteSpace(before.Content))
            {
                note2 = true;
            }
            else if (args.MessageBefore is not null)
            {
                Append("Old Content", "old_content", args.MessageBefore.Content);
            }
            
            if (string.IsNullOrWhiteSpace(args.Message.Content))
            {
                note3 = true;
            }
            else
            {
                var sep = DiscordLoggerEasterEggs.GetSeparator(args.Message.Content, sepstr);
                
                s.AppendLine();
                s.Append("New Content".PadRight(padlen));
                s.Append(sep);
                s.Append(" {new_content}");
                
                o.Add(args.Message.Content);
            }
            
            if (args.Message.Reactions.Count > 0)
            {
                await DiscordLoggerUtilities.ReactionsToString(client, s, o, padlen, sepstr, args.Message);
            }
            
            if (note1)
            {
                Append("Notice", "notice1", "Could not fetch old message content.");
            }
            if (note2)
            {
                Append("Notice", "notice2", "Old message content is empty.");
            }
            if (note3)
            {
                Append("Notice", "notice3", "New message content is empty.");
            }
            
            if (possibleMessageDup)
            {
                Append("Notice", "dup_warning", "This may be a false update event.");
                log.LogDebug(s.ToString(), o.ToArray());
            }
            else
            {
                log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            }
            
            void Append(string name, string tag, object item)
            {
                s!.AppendLine();
                s.Append(name.PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {");
                s.Append(tag);
                s.Append("}");
                
                o!.Add(item);
            }
        }
        
        public async Task HandleDeleted(DiscordClient client, MessageDeleteEventArgs args)
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            
            var padlen = (args.Message.Reactions.Count > 0
                          ? "Reactions".Count()
                          : "Content".Count()) + 1;
            if (args.Message.Attachments.Count > 0)
            {
                padlen = "Attachment".Count() + 1;
            }
            
            if (args.Channel.IsPrivate)
            {
                s.Append("Discord Direct Message Deleted ");
            }
            else
            {
                s.Append("Discord Message Deleted ");
            }
            
            s.Append("({message_id}):");
            o.Add(args.Message.Id);
            
            await DiscordLoggerUtilities.FormatGuildChannelAuthorAttachments(
                client, s, o, padlen, sepstr, args.Message, args.Channel, args.Guild
            );
            
            var note1 = false;
            if (string.IsNullOrWhiteSpace(args.Message.Content))
            {
                note1 = true;
            }
            else
            {
                var sep = DiscordLoggerEasterEggs.GetSeparator(args.Message.Content, sepstr);
                
                s.AppendLine();
                s.Append("Content".PadRight(padlen));
                s.Append(sep);
                s.Append(" {content}");
                
                o.Add(args.Message.Content);
            }
            
            if (args.Message.Reactions.Count > 0)
            {
                await DiscordLoggerUtilities.ReactionsToString(client, s, o, padlen, sepstr, args.Message);
            }
            
            if (note1)
            {
                Append("Notice", "notice1", "Could not fetch message content or message content is empty.");
            }
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            void Append(string name, string tag, object item)
            {
                s!.AppendLine();
                s.Append(name.PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {");
                s.Append(tag);
                s.Append("}");
                
                o!.Add(item);
            }
        }
        
        public Task HandleBulkDelete(DiscordClient client, MessageBulkDeleteEventArgs args)
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            
            if (args.Channel.IsPrivate)
            {
                s.Append("Discord Direct Messages Bulk Deleted");
                s.AppendLine();
                s.Append("From channel {channel_id}");
                o.Add(args.Channel.Id);
            }
            else
            {
                s.Append("Discord Messages Bulk Deleted");
                s.AppendLine();
                s.Append("From channel {channel}");
                o.Add(args.Channel.ToString());
            }
            
            // TODO: log the message too
            s.AppendLine();
            s.Append("Snowflakes of mesages erased:");
            
            foreach (var message in args.Messages)
            {
                s.AppendLine();
                s.Append(" --> ");
                s.Append(message.Id);
                s.Append(" ");
                if (message.Author is not null)
                {
                    s.Append("(Authored by ");
                    s.Append(message.Author.Username);
                    s.Append("#");
                    s.Append(message.Author.Discriminator);
                    s.Append(" ");
                    s.Append(message.Author.Id);
                    s.Append(")");
                }
                else
                {
                    s.Append("(Author unknown)");
                }
            }
            
            // TODO: guess we're not logging this?
            // just remember to s.ToString().ControlCharToSpace()
            
            return Task.CompletedTask;
        }
        
        private async Task<DiscordMessage?> GetBefore(MessageUpdateEventArgs args)
        {
            var before = args.MessageBefore;
            if (before is null)
            {
                try
                {
                    before = await args.Channel.GetMessageAsync(args.Message.Id);
                }
                catch
                {
                    // ignore.
                }
            }
            
            // TODO: if null, get message from database.
            
            return before;
        }
    }
}
