
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Extensions;
    
    public sealed class ChannelEventHandlers
    {
        private ILogger log;
        
        public ChannelEventHandlers(ILogger logger)
        {
            log = logger;
        }
        
        public Task HandleCreated(DiscordClient client, ChannelCreateEventArgs args)
        {
            if (args.Channel.IsPrivate)
            {
                log.LogInformation("Discord private channel created ({id})", args.Channel.Id);
            }
            else
            {
                log.LogInformation(
                    "Discord channel {channel_name} ({channel_id}) created in {guild_name} ({guild_id})",
                    args.Channel.Name.RemoveControlChar(),
                    args.Channel.Id,
                    args.Guild.Name.RemoveControlChar(),
                    args.Guild.Id
                );
            }
            return Task.CompletedTask;
        }
        
        public Task HandleDeleted(DiscordClient client, ChannelDeleteEventArgs args)
        {
            if (args.Channel.IsPrivate)
            {
                log.LogInformation("Discord private channel deleted ({id})", args.Channel.Id);
            }
            else
            {
                log.LogInformation(
                    "Discord channel {channel_name} ({channel_id}) deleted from {guild_name} ({guild_id})",
                    args.Channel.Name.RemoveControlChar(),
                    args.Channel.Id,
                    args.Guild.Name.RemoveControlChar(),
                    args.Guild.Id
                );
            }
            return Task.CompletedTask;
        }
        
        public Task HandleUpdated(DiscordClient client, ChannelUpdateEventArgs args)
        {
            if (
                args.ChannelBefore.Name  == args.ChannelAfter.Name &&
                args.ChannelBefore.Topic == args.ChannelAfter.Topic
            )
            {
                // It's too much effort to handle anything else
                return Task.CompletedTask;
            }
            
            var (s, o) = GetResources();
            
            s.Append("Discord channel {channel_name} ({channel_id}) updated in {guild_name} ({guild_id}):");
            o.Add(args.ChannelBefore.Name);
            o.Add(args.ChannelBefore.Id);
            o.Add(args.Guild.Name);
            o.Add(args.Guild.Id);
            
            if (args.ChannelBefore.Name != args.ChannelAfter.Name)
            {
                s.AppendLine();
                s.Append(" --> Name changed from #{old_name} to #{new_name}");
                o.Add(args.ChannelBefore.Name);
                o.Add(args.ChannelAfter.Name);
            }
            if (args.ChannelBefore.Topic != args.ChannelAfter.Topic)
            {
                s.AppendLine();
                s.Append(" --> ");
                if (string.IsNullOrWhiteSpace(args.ChannelBefore.Topic) && string.IsNullOrWhiteSpace(args.ChannelAfter.Topic))
                {
                    s.Append("Topic before and after is empty. Should not get here.");
                }
                else if (!string.IsNullOrWhiteSpace(args.ChannelBefore.Topic) && string.IsNullOrWhiteSpace(args.ChannelAfter.Topic))
                {
                    s.Append("Topic erased. Old topic was \"{topic}\"");
                    o.Add(args.ChannelBefore.Topic);
                }
                else if (string.IsNullOrWhiteSpace(args.ChannelBefore.Topic) && !string.IsNullOrWhiteSpace(args.ChannelAfter.Topic))
                {
                    s.Append("Topic set to \"{new_topic}\"");
                    o.Add(args.ChannelAfter.Topic);
                }
                else if (!string.IsNullOrWhiteSpace(args.ChannelBefore.Topic) && !string.IsNullOrWhiteSpace(args.ChannelAfter.Topic))
                {
                    s.Append("Topic changed from \"{old_topic}\" to \"{new_topic}\"");
                    o.Add(args.ChannelBefore.Topic);
                    o.Add(args.ChannelAfter.Topic);
                }
            }
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            return Task.CompletedTask;
        }
        
        public Task HandlePinsUpdated(DiscordClient client, ChannelPinsUpdateEventArgs args)
        {
            // Not implementing at the moment.
            return Task.CompletedTask;
        }
        
        public Task HandleDmDeleted(DiscordClient client, DmChannelDeleteEventArgs args)
        {
            log.LogInformation("Discord private channel deleted ({id})", args.Channel.Id);
            return Task.CompletedTask;
        }
        
        private (StringBuilder, List<object>) GetResources()
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            return (s, o);
        }
    }
}
