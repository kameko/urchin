
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    internal sealed class LoggingDiscordEventHandlers
    {
        private string sepstr;
        private ILogger log;
        private ExceptionListener listener;
        private MessageEventHandlers messageHandler;
        private MessageReactionEventHandlers messageReactionHandler;
        private GuildEventHandlers guildHandler;
        private ChannelEventHandlers channelHandler;
        private VoiceEventHandlers voiceHandlers;
        private IntegrationsEventHandlers integrationsHandlers;
        private UserEventHandlers userHandlers;
        private SocketEventHandlers socketHandlers;
        private ClientEventHandlers clientHandlers;
        
        public LoggingDiscordEventHandlers(ExceptionListener exceptionListener, ILogger logger)
        {
            // ¥ ☺ ☻ ♥ ♦ ♣ ♠ • ○ ♂ ♀ ♪ ♫ ☼ ► ◄ ▲ ▼ ‼ ¶ § ▬ ↑ ↓ → ← ↔ √ ∩ ± ≥ ≤ ÷ ≈ 
            sepstr   = "¥";
            listener = exceptionListener;
            log      = logger;
            
            messageHandler         = new(logger, sepstr);
            messageReactionHandler = new(logger, sepstr);
            guildHandler           = new(logger);
            channelHandler         = new(logger);
            voiceHandlers          = new(logger, sepstr);
            integrationsHandlers   = new(logger);
            userHandlers           = new(logger);
            socketHandlers         = new(logger);
            clientHandlers         = new(logger);
        }
        
        private Task Run(DiscordClient client, Func<Task> callback)
        {
            // This is to handle a bug in the Emzi0767 async event class,
            // which not only swallows exceptions but also appears to make
            // my program horribly misbehave, including not shutting down.
            // See Also: https://github.com/DSharpPlus/DSharpPlus/issues/875 
            return Task.Run(async () =>
            {
                await callback.Invoke();
            })
            .ContinueWith(async task =>
            {
                // TODO: aside from logging, maybe we want to swallow exceptions?
                if (task.IsFaulted)
                {
                    // And when people question the validity of my ExceptionListener
                    // class, this is *exactly* what it was designed to fix.
                    listener.Tell(task.Exception!);
                    await client.DisconnectAsync();
                }
            });
        }
        
        public void HookAll(DiscordClient client)
        {
            client.MessageAcknowledged += MessageAcknowledged;
            client.MessageCreated      += MessageCreated;
            client.MessageUpdated      += MessageUpdated;
            client.MessageDeleted      += MessageDeleted;
            client.MessagesBulkDeleted += MessagesBulkDelete;
            
            client.MessageReactionAdded        += MessageReactionAdd;
            client.MessageReactionRemoved      += MessageReactionRemoved;
            client.MessageReactionRemovedEmoji += MessageReactionRemovedEmoji;
            client.MessageReactionsCleared     += MessageReactionClear;
            
            client.GuildDownloadCompleted   += GuildDownloadCompleted;
            client.GuildIntegrationsUpdated += GuildIntegrationsUpdated;
            client.GuildAvailable           += GuildAvailable;
            client.GuildUnavailable         += GuildUnavailable;
            client.GuildUpdated             += GuildUpdated;
            client.GuildBanAdded            += GuildBanAdded;
            client.GuildBanRemoved          += GuildBanRemoved;
            client.GuildCreated             += GuildCreated;
            client.GuildDeleted             += GuildDeleted;
            client.GuildEmojisUpdated       += GuildEmojiUpdated;
            client.GuildMemberAdded         += GuildMemberAdded;
            client.GuildMemberRemoved       += GuildMemberRemoved;
            client.GuildMemberUpdated       += GuildMemberUpdated;
            client.GuildRoleCreated         += GuildRoleCreated;
            client.GuildRoleDeleted         += GuildRoleDeleted;
            client.GuildRoleUpdated         += GuildRoleUpdated;
            client.GuildStickersUpdated     += GuildStickersUpdated;
            client.InviteCreated            += GuildInviteCreated;
            client.InviteDeleted            += GuildInviteDeleted;
            
            client.ChannelCreated     += ChannelCreated;
            client.ChannelDeleted     += ChannelDeleted;
            client.ChannelUpdated     += ChannelUpdated;
            client.ChannelPinsUpdated += ChannelPinsUpdated;
            client.DmChannelDeleted   += ChannelDmDeleted;
            
            client.VoiceServerUpdated += VoiceServerUpdated;
            client.VoiceStateUpdated  += VoiceStateUpdated;
            
            client.IntegrationCreated += IntegrationsCreated;
            client.IntegrationDeleted += IntegrationsDeleted;
            client.IntegrationUpdated += IntegrationsUpdated;
            
            client.PresenceUpdated     += PresenceUpdated;
            client.UserSettingsUpdated += UserSettingsUpdated;
            client.UserUpdated         += UserUpdated;
            
            client.WebhooksUpdated += WebhooksUpdated;
            client.SocketOpened    += SocketOpened;
            client.SocketClosed    += SocketClosed;
            client.SocketErrored   += SocketErrored;
            
            client.Ready        += Ready;
            client.Resumed      += Resumed;
            client.UnknownEvent += UnknownEvent;
        }
        
        public Task SendingMessage(DiscordClient client, DiscordChannel channel, DiscordMessageBuilder message) =>
            Run(client, () => messageHandler.HandleSending(client, channel, message));
        
        public Task MessageAcknowledged(DiscordClient client, MessageAcknowledgeEventArgs args) =>
            Run(client, () => messageHandler.HandleAcknowledged(client, args));
        public Task MessageCreated(DiscordClient client, MessageCreateEventArgs args) =>
            Run(client, () => messageHandler.HandleCreated(client, args));
        public Task MessageUpdated(DiscordClient client, MessageUpdateEventArgs args) =>
            Run(client, () => messageHandler.HandleUpdated(client, args));
        public Task MessageDeleted(DiscordClient client, MessageDeleteEventArgs args) =>
            Run(client, () => messageHandler.HandleDeleted(client, args));
        public Task MessagesBulkDelete(DiscordClient client, MessageBulkDeleteEventArgs args) =>
            Run(client, () => messageHandler.HandleBulkDelete(client, args));
        
        public Task MessageReactionAdd(DiscordClient client, MessageReactionAddEventArgs args) =>
            Run(client, () => messageReactionHandler.HandleAdd(client, args));
        public Task MessageReactionRemoved(DiscordClient client, MessageReactionRemoveEventArgs args) =>
            Run(client, () => messageReactionHandler.HandleRemoved(client, args));
        public Task MessageReactionRemovedEmoji(DiscordClient client, MessageReactionRemoveEmojiEventArgs args) =>
            Run(client, () => messageReactionHandler.HandleRemovedEmoji(client, args));
        public Task MessageReactionClear(DiscordClient client, MessageReactionsClearEventArgs args) =>
            Run(client, () => messageReactionHandler.HandleClear(client, args));
        
        public Task GuildDownloadCompleted(DiscordClient client, GuildDownloadCompletedEventArgs args) =>
            Run(client, () => guildHandler.HandleDownloadCompleted(client, args));
        public Task GuildIntegrationsUpdated(DiscordClient client, GuildIntegrationsUpdateEventArgs args) =>
            Run(client, () => guildHandler.HandleIntegrationsUpdated(client, args));
        public Task GuildAvailable(DiscordClient client, GuildCreateEventArgs args) =>
            Run(client, () => guildHandler.HandleAvailable(client, args));
        public Task GuildUnavailable(DiscordClient client, GuildDeleteEventArgs args) =>
            Run(client, () => guildHandler.HandleUnavailable(client, args));
        public Task GuildUpdated(DiscordClient client, GuildUpdateEventArgs args) =>
            Run(client, () => guildHandler.HandleUpdated(client, args));
        public Task GuildBanAdded(DiscordClient client, GuildBanAddEventArgs args) =>
            Run(client, () => guildHandler.HandleBanAdded(client, args));
        public Task GuildBanRemoved(DiscordClient client, GuildBanRemoveEventArgs args) =>
            Run(client, () => guildHandler.HandleBanRemoved(client, args));
        public Task GuildCreated(DiscordClient client, GuildCreateEventArgs args) =>
            Run(client, () => guildHandler.HandleCreated(client, args));
        public Task GuildDeleted(DiscordClient client, GuildDeleteEventArgs args) =>
            Run(client, () => guildHandler.HandleDeleted(client, args));
        public Task GuildEmojiUpdated(DiscordClient client, GuildEmojisUpdateEventArgs args) =>
            Run(client, () => guildHandler.HandleEmojiUpdated(client, args));
        public Task GuildMemberAdded(DiscordClient client, GuildMemberAddEventArgs args) =>
            Run(client, () => guildHandler.HandleMemberAdded(client, args));
        public Task GuildMemberRemoved(DiscordClient client, GuildMemberRemoveEventArgs args) =>
            Run(client, () => guildHandler.HandleMemberRemoved(client, args));
        public Task GuildMemberUpdated(DiscordClient client, GuildMemberUpdateEventArgs args) =>
            Run(client, () => guildHandler.HandleMemberUpdated(client, args));
        public Task GuildRoleCreated(DiscordClient client, GuildRoleCreateEventArgs args) =>
            Run(client, () => guildHandler.HandleRoleCreated(client, args));
        public Task GuildRoleDeleted(DiscordClient client, GuildRoleDeleteEventArgs args) =>
            Run(client, () => guildHandler.HandleRoleDeleted(client, args));
        public Task GuildRoleUpdated(DiscordClient client, GuildRoleUpdateEventArgs args) =>
            Run(client, () => guildHandler.HandleRoleUpdated(client, args));
        public Task GuildStickersUpdated(DiscordClient client, GuildStickersUpdateEventArgs args) =>
            Run(client, () => guildHandler.HandleStickersUpdated(client, args));
        public Task GuildInviteCreated(DiscordClient client, InviteCreateEventArgs args) =>
            Run(client, () => guildHandler.HandleInviteCreated(client, args));
        public Task GuildInviteDeleted(DiscordClient client, InviteDeleteEventArgs args) =>
            Run(client, () => guildHandler.HandleInviteDeleted(client, args));
        
        public Task ChannelCreated(DiscordClient client, ChannelCreateEventArgs args) =>
            Run(client, () => channelHandler.HandleCreated(client, args));
        public Task ChannelDeleted(DiscordClient client, ChannelDeleteEventArgs args) =>
            Run(client, () => channelHandler.HandleDeleted(client, args));
        public Task ChannelUpdated(DiscordClient client, ChannelUpdateEventArgs args) =>
            Run(client, () => channelHandler.HandleUpdated(client, args));
        public Task ChannelPinsUpdated(DiscordClient client, ChannelPinsUpdateEventArgs args) =>
            Run(client, () => channelHandler.HandlePinsUpdated(client, args));
        public Task ChannelDmDeleted(DiscordClient client, DmChannelDeleteEventArgs args) =>
            Run(client, () => channelHandler.HandleDmDeleted(client, args));
        
        public Task VoiceServerUpdated(DiscordClient client, VoiceServerUpdateEventArgs args) =>
            Run(client, () => voiceHandlers.HandleServerUpdated(client, args));
        public Task VoiceStateUpdated(DiscordClient client, VoiceStateUpdateEventArgs args) =>
            Run(client, () => voiceHandlers.HandleStateUpdated(client, args));
        
        public Task IntegrationsCreated(DiscordClient client, IntegrationCreateEventArgs args) =>
            Run(client, () => integrationsHandlers.HandleCreated(client, args));
        public Task IntegrationsDeleted(DiscordClient client, IntegrationDeleteEventArgs args) =>
            Run(client, () => integrationsHandlers.HandleDeleted(client, args));
        public Task IntegrationsUpdated(DiscordClient client, IntegrationUpdateEventArgs args) =>
            Run(client, () => integrationsHandlers.HandleUpdated(client, args));
        
        public Task PresenceUpdated(DiscordClient client, PresenceUpdateEventArgs args) =>
            Run(client, () => userHandlers.HandlePresenceUpdated(client, args));
        public Task UserSettingsUpdated(DiscordClient client, UserSettingsUpdateEventArgs args) =>
            Run(client, () => userHandlers.HandleSettingsUpdated(client, args));
        public Task UserUpdated(DiscordClient client, UserUpdateEventArgs args) =>
            Run(client, () => userHandlers.HandleUpdated(client, args));
        
        public Task WebhooksUpdated(DiscordClient client, WebhooksUpdateEventArgs args) =>
            Run(client, () => socketHandlers.HandleWebhooksUpdated(client, args));
        public Task SocketOpened(DiscordClient client, SocketEventArgs args) =>
            Run(client, () => socketHandlers.HandleOpened(client, args));
        public Task SocketClosed(DiscordClient client, SocketCloseEventArgs args) =>
            Run(client, () => socketHandlers.HandleClosed(client, args));
        public Task SocketErrored(DiscordClient client, SocketErrorEventArgs args) =>
            Run(client, () => socketHandlers.HandleErrored(client, args));
        
        public Task Ready(DiscordClient client, ReadyEventArgs args) =>
            Run(client, () => clientHandlers.HandleReady(client, args));
        public Task Resumed(DiscordClient client, ReadyEventArgs args) =>
            Run(client, () => clientHandlers.HandleResumed(client, args));
        public Task UnknownEvent(DiscordClient client, UnknownEventArgs args) =>
            Run(client, () => clientHandlers.HandleUnknown(client, args));
    }
}
