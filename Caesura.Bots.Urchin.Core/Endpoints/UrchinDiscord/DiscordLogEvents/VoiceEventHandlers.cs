
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Extensions;
    
    public sealed class VoiceEventHandlers
    {
        private ILogger log;
        private string sepstr;
        
        public VoiceEventHandlers(ILogger logger, string separator)
        {
            log    = logger;
            sepstr = separator;
        }
        
        public Task HandleServerUpdated(DiscordClient client, VoiceServerUpdateEventArgs args)
        {
            log.LogInformation(
                "Discord voice server endpoint in {guild_name} ({guild_id}) updated:" + Environment.NewLine +
                " --> {endpoint}",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id,
                args.Endpoint
            );
            return Task.CompletedTask;
        }
        
        public Task HandleStateUpdated(DiscordClient client, VoiceStateUpdateEventArgs args)
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            
            var padlen = (args.Channel is null ? "Guild".Length : "Channel".Length) + 1;
            
            s.Append("Discord voice state updated:");
            
            if (args.Guild is not null)
            {
                s.AppendLine();
                s.Append("Guild".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {guild_name} ({guild_id})");
                o.Add(args.Guild.Name);
                o.Add(args.Guild.Id);
            }
            
            if (args.Channel is not null)
            {
                s.AppendLine();
                s.Append("Channel".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {channel_name} ({channel_id})");
                o.Add(args.Channel.Name);
                o.Add(args.Channel.Id);
            }
            
            if (args.User is not null)
            {
                s.AppendLine();
                s.Append("User".PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {user_name} ({user_id})");
                o.Add(DiscordLoggerUtilities.GetUsername(args.User));
                o.Add(args.User.Id);
            }
            
            s.AppendLine();
            s.Append("Event".PadRight(padlen));
            s.Append(sepstr);
            s.Append(" ");
            
            var event_happened = false;
            if (args.Before is not null && args.After is not null)
            {
                if (args.Before.IsSuppressed != args.After.IsSuppressed)
                {
                    s.Append(
                        args.After.IsSuppressed
                        ? "Suppressed"
                        : "Unsppressed"  
                    );
                    event_happened = true;
                }
                if (args.Before.IsServerDeafened != args.After.IsServerDeafened)
                {
                    s.Append(
                        args.After.IsServerDeafened
                        ? "Server Deafened"
                        : "Server Undeafened"  
                    );
                    event_happened = true;
                }
                if (args.Before.IsServerMuted != args.After.IsServerMuted)
                {
                    s.Append(
                        args.After.IsServerMuted
                        ? "Server Muted"
                        : "Server Unmuted"  
                    );
                    event_happened = true;
                }
                if (args.Before.IsSelfDeafened != args.After.IsSelfDeafened)
                {
                    s.Append(
                        args.After.IsSelfDeafened
                        ? "Self Deafened"
                        : "Self Undeafened"  
                    );
                    event_happened = true;
                }
                if (args.Before.IsSelfMuted != args.After.IsSelfMuted)
                {
                    s.Append(
                        args.After.IsSelfMuted
                        ? "Self Muted"
                        : "Self Unmuted"  
                    );
                    event_happened = true;
                }
                if (args.Before.IsSelfStream != args.After.IsSelfStream)
                {
                    s.Append(
                        args.After.IsSelfStream
                        ? "Streaming"
                        : "Stopped streaming"  
                    );
                    event_happened = true;
                }
                if (args.Before.IsSelfVideo != args.After.IsSelfVideo)
                {
                    s.Append(
                        args.After.IsSelfVideo
                        ? "Camera is live"
                        : "Camera is no longer live"  
                    );
                    event_happened = true;
                }
            }
            else if (args.Channel is not null && args.Channel.Users.Any(x => x.Id == (args.User?.Id ?? 0)))
            {
                s.Append("User joined");
                event_happened = true;
            }
            
            if (!event_happened)
            {
                s.Append("User left");
            }
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            return Task.CompletedTask;
        }
    }
}
