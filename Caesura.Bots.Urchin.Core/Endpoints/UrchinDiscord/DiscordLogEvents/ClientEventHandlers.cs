
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    
    public sealed class ClientEventHandlers
    {
        private ILogger log;
        
        public ClientEventHandlers(ILogger logger)
        {
            log = logger;
        }
        
        public Task HandleReady(DiscordClient client, ReadyEventArgs args)
        {
            log.LogInformation("Discord client ready");
            return Task.CompletedTask;
        }
        
        public Task HandleResumed(DiscordClient client, ReadyEventArgs args)
        {
            log.LogInformation("Discord client resumed");
            return Task.CompletedTask;
        }
        
        public Task HandleUnknown(DiscordClient client, UnknownEventArgs args)
        {
            log.LogInformation(
                "Unknown Discord event: {event_name}" + Environment.NewLine +
                " --> {event_json}",
                args.EventName,
                args.Json
            );
            return Task.CompletedTask;
        }
    }
}
