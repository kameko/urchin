
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    
    public sealed class IntegrationsEventHandlers
    {
        private ILogger log;
        
        public IntegrationsEventHandlers(ILogger logger)
        {
            log = logger;
        }
        
        public Task HandleCreated(DiscordClient client, IntegrationCreateEventArgs args)
        {
            // Unimplemented.
            return Task.CompletedTask;
        }
        
        public Task HandleDeleted(DiscordClient client, IntegrationDeleteEventArgs args)
        {
            // Unimplemented.
            return Task.CompletedTask;
        }
        
        public Task HandleUpdated(DiscordClient client, IntegrationUpdateEventArgs args)
        {
            // Unimplemented.
            return Task.CompletedTask;
        }
    }
}
