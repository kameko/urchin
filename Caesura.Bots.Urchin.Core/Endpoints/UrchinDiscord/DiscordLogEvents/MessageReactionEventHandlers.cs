
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Extensions;
    
    internal sealed class MessageReactionEventHandlers
    {
        private ILogger log;
        private string sepstr;
        
        public MessageReactionEventHandlers(ILogger logger, string separator)
        {
            log    = logger;
            sepstr = separator;
        }
        
        public async Task HandleAdd(DiscordClient client, MessageReactionAddEventArgs args)
        {
            var (s, o) = GetResources();
            
            FormatHeader(s, o, args.Message, args.Channel);
            
            FormatAddOrRemove(s, o, args.Emoji, args.User, "Addded");
            
            await FormatRest(client, s, o, args.Message, args.Channel, args.Guild);
            
            log.LogInformation(s.ToString(), o.ToArray());
        }
        
        public async Task HandleRemoved(DiscordClient client, MessageReactionRemoveEventArgs args)
        {
            var (s, o) = GetResources();
            
            FormatHeader(s, o, args.Message, args.Channel);
            
            FormatAddOrRemove(s, o, args.Emoji, args.User, "Removed");
            
            await FormatRest(client, s, o, args.Message, args.Channel, args.Guild);
            
            log.LogInformation(s.ToString(), o.ToArray());
        }
        
        public async Task HandleRemovedEmoji(DiscordClient client, MessageReactionRemoveEmojiEventArgs args)
        {
            var (s, o) = GetResources();
            
            FormatHeader(s, o, args.Message, args.Channel);
            
            s.AppendLine();
            s.Append("All reactions of kind {reaction} removed.");
            o.Add(args.Emoji.GetDiscordName());
            if (args.Emoji.Id > 0)
            {
                s.AppendLine();
                s.Append("URL: {reaction_url}");
                o.Add(args.Emoji.Url);
            }
            
            await FormatRest(client, s, o, args.Message, args.Channel, args.Guild);
            
            log.LogInformation(s.ToString(), o.ToArray());
        }
        
        public async Task HandleClear(DiscordClient client, MessageReactionsClearEventArgs args)
        {
            var (s, o) = GetResources();
            
            FormatHeader(s, o, args.Message, args.Channel);
            
            s.AppendLine();
            s.Append("All reactions cleared.");
            
            await FormatRest(client, s, o, args.Message, args.Channel, args.Guild);
            
            log.LogInformation(s.ToString(), o.ToArray());
        }
        
        private (StringBuilder, List<object>) GetResources()
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            return (s, o);
        }
        
        private void FormatHeader(
            StringBuilder s,
            List<object> o,
            DiscordMessage message,
            DiscordChannel channel
        )
        {
            if (channel.IsPrivate)
            {
                s.Append("Discord Direct Message ");
            }
            else
            {
                s.Append("Discord Message ");
            }
            
            s.Append("({message_id}):");
            o.Add(message.Id);
        }
        
        private void FormatAddOrRemove(StringBuilder s, List<object> o, DiscordEmoji emoji, DiscordUser? user, string action)
        {
            s.AppendLine();
            s.Append("Reaction ");
            s.Append(action);
            s.Append(": {reaction}");
            o.Add(emoji.GetDiscordName());
            
            if (emoji.Id > 0)
            {
                s.AppendLine();
                s.Append("URL: {reaction_url}");
                o.Add(emoji.Url);
            }
            
            if (user is not null)
            {
                s.AppendLine();
                s.Append(action);
                s.Append(" by {user_name} ({user_id})");
                o.Add(DiscordLoggerUtilities.GetUsername(user));
                o.Add(user.Id);
            }
            else
            {
                s.AppendLine();
                s.Append("Could not fetch user.");
            }
        }
        
        private async Task FormatRest(
            DiscordClient client,
            StringBuilder s,
            List<object> o,
            DiscordMessage message,
            DiscordChannel channel,
            DiscordGuild? guild
        )
        {
            var padlen = (message.Reactions.Count > 0
                          ? "Reactions".Count()
                          : "Content".Count()) + 1;
            if (message.Attachments.Count > 0)
            {
                padlen = "Attachment".Count();
            }
            
            await DiscordLoggerUtilities.FormatGuildChannelAuthorAttachments(
                client, s, o, padlen, sepstr, message, channel, guild
            );
            
            var note1 = false;
            if (string.IsNullOrWhiteSpace(message.Content))
            {
                note1 = true;
            }
            else
            {
                var sep = DiscordLoggerEasterEggs.GetSeparator(message.Content, sepstr);
                
                s.AppendLine();
                s.Append("Content".PadRight(padlen));
                s.Append(sep);
                s.Append(" {content}");
                
                o.Add(message.Content);
            }
            
            if (message.Reactions.Count > 0)
            {
                await DiscordLoggerUtilities.ReactionsToString(client, s, o, padlen, sepstr, message);
            }
            
            if (note1)
            {
                Append("Notice", "notice1", "Could not fetch message content or message content is empty.");
            }
            
            void Append(string name, string tag, object item)
            {
                s!.AppendLine();
                s.Append(name.PadRight(padlen));
                s.Append(sepstr);
                s.Append(" {");
                s.Append(tag);
                s.Append("}");
                
                o!.Add(item);
            }
        }
    }
}
