
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Extensions;
    
    public sealed class GuildEventHandlers
    {
        private ILogger log;
        
        public GuildEventHandlers(ILogger logger)
        {
            log = logger;
        }
        
        public Task HandleDownloadCompleted(DiscordClient client, GuildDownloadCompletedEventArgs args)
        {
            var (s, o) = GetResources();
            
            s.Append("Guilds available:");
            foreach (var (id, guild) in args.Guilds)
            {
                s.AppendLine();
                s.Append(" --> {name} ({id})");
                o.Add(guild.Name);
                o.Add(guild.Id);
            }
            
            return Task.CompletedTask;
        }
        
        public Task HandleIntegrationsUpdated(DiscordClient client, GuildIntegrationsUpdateEventArgs args)
        {
            log.LogWarning("Guild integrations log event not implemented.");
            return Task.CompletedTask;
        }
        
        public Task HandleAvailable(DiscordClient client, GuildCreateEventArgs args)
        {
            log.LogInformation(
                "Discord guild available: {name} ({id})",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleUnavailable(DiscordClient client, GuildDeleteEventArgs args)
        {
            log.LogInformation(
                "Discord guild not available: {name} ({id})",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleUpdated(DiscordClient client, GuildUpdateEventArgs args)
        {
            if (args.GuildAfter.Name != args.GuildBefore.Name)
            {
                log.LogInformation(
                    "Discord guild updated: {name} ({id}) (Previously named {old_name})",
                    args.GuildAfter.Name.RemoveControlChar(),
                    args.GuildAfter.Id,
                    args.GuildBefore.Name.RemoveControlChar()
                );
            }
            return Task.CompletedTask;
        }
        
        public Task HandleBanAdded(DiscordClient client, GuildBanAddEventArgs args)
        {
            log.LogInformation(
                "Discord user {user_name} ({user_id}) banned from {guild_name} ({guild_id})",
                args.Member.GetUsername(),
                args.Member.Id,
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleBanRemoved(DiscordClient client, GuildBanRemoveEventArgs args)
        {
            log.LogInformation(
                "Discord user {user_name} ({user_id}) unbanned from {guild_name} ({guild_id})",
                args.Member.GetUsername(),
                args.Member.Id,
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleCreated(DiscordClient client, GuildCreateEventArgs args)
        {
            log.LogInformation(
                "Discord guild created: {name} ({id})",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleDeleted(DiscordClient client, GuildDeleteEventArgs args)
        {
            log.LogInformation(
                "Discord guild deleted: {name} ({id})",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleEmojiUpdated(DiscordClient client, GuildEmojisUpdateEventArgs args)
        {
            var eb = args.EmojisBefore;
            var ea = args.EmojisAfter;
            
            if (eb.Count > ea.Count) // emoji deleted
            {
                var deleted = eb.Except(ea);
                
                foreach (var (id, emoji) in deleted)
                {
                    log.LogInformation(
                        "Emoji {emoji_name} ({emoji_id}) deleted from {guild_name} ({guild_id})",
                        emoji.GetDiscordName().RemoveControlChar(),
                        emoji.Id,
                        args.Guild.Name.RemoveControlChar(),
                        args.Guild.Id
                    );
                }
            }
            else if (eb.Count < ea.Count) // emoji added
            {
                var added = ea.Except(eb);
                
                foreach (var (id, emoji) in added)
                {
                    log.LogInformation(
                        "Emoji {emoji_name} ({emoji_id}) added to {guild_name} ({guild_id})"
                        + Environment.NewLine + " --> URL: {url}",
                        emoji.GetDiscordName().RemoveControlChar(),
                        emoji.Id,
                        args.Guild.Name.RemoveControlChar(),
                        args.Guild.Id,
                        emoji.Url
                    );
                }
            }
            else // emoji changed
            {
                foreach (var (id, emoji) in args.EmojisAfter)
                {
                    var success = args.EmojisBefore.TryGetValue(id, out var before);
                    if (success)
                    {
                        log.LogInformation(
                            "Emoji {old_emoji_name} ({emoji_id}) in {guild_name} ({guild_id}) renamed to {new_emoji_name}"
                            + Environment.NewLine + " --> URL: {url}",
                            before!.GetDiscordName().RemoveControlChar(),
                            emoji.Id,
                            args.Guild.Name.RemoveControlChar(),
                            args.Guild.Id,
                            emoji.GetDiscordName().RemoveControlChar(),
                            emoji.Url
                        );
                    }
                }
            }
            
            return Task.CompletedTask;
        }
        
        public Task HandleMemberAdded(DiscordClient client, GuildMemberAddEventArgs args)
        {
            log.LogInformation(
                "Discord user {user_name} ({user_id}) joined {guild_name} ({guild_id})",
                args.Member.GetUsername(),
                args.Member.Id,
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleMemberRemoved(DiscordClient client, GuildMemberRemoveEventArgs args)
        {
            log.LogInformation(
                "Discord user {user_name} ({user_id}) left {guild_name} ({guild_id})",
                args.Member.GetUsername(),
                args.Member.Id,
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id
            );
            return Task.CompletedTask;
        }
        
        public Task HandleMemberUpdated(DiscordClient client, GuildMemberUpdateEventArgs args)
        {
            var (s, o) = GetResources();
            
            if (args.NicknameBefore != args.NicknameAfter)
            {
                s.Append("Discord user {user_name} ({user_id}) in {guild_name} ({guild_id}):");
                o.Add(args.Member.GetUsername());
                o.Add(args.Member.Id);
                o.Add(args.Guild.Name);
                o.Add(args.Guild.Id);
                
                if (string.IsNullOrEmpty(args.NicknameBefore) && !string.IsNullOrEmpty(args.NicknameAfter))
                {
                    s.AppendLine();
                    s.Append(" --> Nickname set to \"{nickname}\"");
                    o.Add(args.NicknameAfter);
                }
                else if (!string.IsNullOrEmpty(args.NicknameBefore) && string.IsNullOrEmpty(args.NicknameAfter))
                {
                    s.AppendLine();
                    s.Append(" --> Nickname reset from \"{nickname}\"");
                    o.Add(args.NicknameBefore);
                }
                else if (!string.IsNullOrEmpty(args.NicknameBefore) && !string.IsNullOrEmpty(args.NicknameAfter))
                {
                    s.AppendLine();
                    s.Append(" --> Nickname changed from \"{old_nickname}\" to \"{new_nickname}\"");
                    o.Add(args.NicknameBefore);
                    o.Add(args.NicknameAfter);
                }
                
                log.LogInformation(s.ToString(), o.ToArray());
            }
            
            if (args.RolesBefore.Count != args.RolesAfter.Count)
            {
                s.Clear();
                o.Clear();
                
                s.Append("Discord user {user_name} ({user_id}) in {guild_name} ({guild_id}):");
                o.Add(args.Member.GetUsername());
                o.Add(args.Member.Id);
                o.Add(args.Guild.Name);
                o.Add(args.Guild.Id);
                
                s.AppendLine();
                s.Append(" --> Roles Before: ");
                var old_role_count = 0;
                foreach (var old_role in args.RolesBefore)
                {
                    s.Append("{old_role_name_");
                    s.Append(old_role_count);
                    s.Append("} ({old_role_id_");
                    s.Append(old_role_count);
                    s.Append("}) ");
                    
                    o.Add(old_role.Name);
                    o.Add(old_role.Id);
                    
                    old_role_count++;
                }
                
                s.AppendLine();
                s.Append(" --> Roles After: ");
                var new_role_count = 0;
                foreach (var new_role in args.RolesAfter)
                {
                    s.Append("{old_role_name_");
                    s.Append(new_role_count);
                    s.Append("} ({old_role_id_");
                    s.Append(new_role_count);
                    s.Append("}) ");
                    
                    o.Add(new_role.Name);
                    o.Add(new_role.Id);
                    
                    new_role_count++;
                }
                
                log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            }
            
            return Task.CompletedTask;
        }
        
        public Task HandleRoleCreated(DiscordClient client, GuildRoleCreateEventArgs args)
        {
            var (s, o) = GetResources();
            
            s.Append("Discord role {role_name} ({role_id}) created in {guild_name} ({guild_id})");
            o.Add(args.Role.Name);
            o.Add(args.Role.Id);
            o.Add(args.Guild.Name);
            o.Add(args.Guild.Id);
            
            s.AppendLine();
            s.Append(" --> Permissions: {permissions}");
            o.Add(args.Role.Permissions);
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            return Task.CompletedTask;
        }
        
        public Task HandleRoleDeleted(DiscordClient client, GuildRoleDeleteEventArgs args)
        {
            
            var (s, o) = GetResources();
            
            s.Append("Discord role {role_name} ({role_id}) deleted from {guild_name} ({guild_id})");
            o.Add(args.Role.Name);
            o.Add(args.Role.Id);
            o.Add(args.Guild.Name);
            o.Add(args.Guild.Id);
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            return Task.CompletedTask;
        }
        
        public Task HandleRoleUpdated(DiscordClient client, GuildRoleUpdateEventArgs args)
        {
            var (s, o) = GetResources();
            
            s.Append("Discord role {role_name} ({role_id}) modified in {guild_name} ({guild_id})");
            o.Add(args.RoleBefore.Name);
            o.Add(args.RoleBefore.Id);
            o.Add(args.Guild.Name);
            o.Add(args.Guild.Id);
            
            if (args.RoleBefore.Name != args.RoleAfter.Name)
            {
                s.AppendLine();
                s.Append(" --> Name changed from \"{old_name}\" to \"{new_name}\"");
                o.Add(args.RoleBefore.Name);
                o.Add(args.RoleAfter.Name);
            }
            
            if (args.RoleBefore.Permissions == args.RoleAfter.Permissions)
            {
                s.AppendLine();
                s.Append(" --> Permissions: {permissions}");
                o.Add(args.RoleAfter.Permissions);
            }
            else
            {
                s.AppendLine();
                s.Append(" --> Permissions Before: {old_permissions}");
                o.Add(args.RoleBefore.Permissions);
                
                s.AppendLine();
                s.Append(" --> Permissions After: {new_permissions}");
                o.Add(args.RoleAfter.Permissions);
            }
            
            log.LogInformation(s.ToString().RemoveControlChar(), o.ToArray());
            
            return Task.CompletedTask;
        }
        
        public Task HandleStickersUpdated(DiscordClient client, GuildStickersUpdateEventArgs args)
        {
            log.LogInformation("Stickers updated, logger not implemented.");
            return Task.CompletedTask;
        }
        
        public Task HandleInviteCreated(DiscordClient client, InviteCreateEventArgs args)
        {
            log.LogInformation(
                "Discord invite for guild {guild_name} ({guild_id}) created." + Environment.NewLine +
                " --> Inviter  : {creator}" + Environment.NewLine +
                " --> Code     : {code}" + Environment.NewLine +
                " --> Max uses : {uses}" + Environment.NewLine +
                " --> Expires  : {expire_date}",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id,
                args.Invite.Inviter.GetUsername(),
                args.Invite.Code,
                args.Invite.MaxUses,
                args.Invite.ExpiresAt
            );
            return Task.CompletedTask;
        }
        
        public Task HandleInviteDeleted(DiscordClient client, InviteDeleteEventArgs args)
        {
            log.LogInformation(
                "Discord invite for guild {guild_name} ({guild_id}) deleted." + Environment.NewLine +
                " --> Inviter: {creator}",
                args.Guild.Name.RemoveControlChar(),
                args.Guild.Id,
                args.Invite.Inviter.GetUsername()
            );
            return Task.CompletedTask;
        }
        
        private (StringBuilder, List<object>) GetResources()
        {
            var s = new StringBuilder(60);
            var o = new List<object>(8);
            return (s, o);
        }
    }
}
