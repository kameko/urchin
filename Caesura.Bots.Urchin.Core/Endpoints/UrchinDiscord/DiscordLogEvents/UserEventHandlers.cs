
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordLogEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Extensions;
    
    public sealed class UserEventHandlers
    {
        private ILogger log;
        
        public UserEventHandlers(ILogger logger)
        {
            log = logger;
        }
        
        public Task HandlePresenceUpdated(DiscordClient client, PresenceUpdateEventArgs args)
        {
            // Unimplemented.
            return Task.CompletedTask;
        }
        
        public Task HandleSettingsUpdated(DiscordClient client, UserSettingsUpdateEventArgs args)
        {
            // Unimplemented.
            return Task.CompletedTask;
        }
        
        public Task HandleUpdated(DiscordClient client, UserUpdateEventArgs args)
        {
            if (args.UserBefore is not null && args.UserAfter is not null)
            {
                if (args.UserBefore.Username != args.UserAfter.Username)
                {
                    log.LogInformation(
                        "Discord user updated: {username} ({id}) changed username to {new_name}",
                        DiscordLoggerUtilities.GetUsername(args.UserBefore),
                        args.UserBefore.Id,
                        args.UserAfter.Username.RemoveControlChar()
                    );
                }
                if (args.UserBefore.Discriminator != args.UserAfter.Discriminator)
                {
                    log.LogInformation(
                        "Discord user updated: {username} ({id}) changed discriminator to {new_discriminator}",
                        DiscordLoggerUtilities.GetUsername(args.UserBefore),
                        args.UserBefore.Id,
                        args.UserAfter.Discriminator
                    );
                }
                if (args.UserBefore.AvatarHash != args.UserAfter.AvatarHash)
                {
                    log.LogInformation(
                        "Discord user updated: {username} ({id}) updated avatar" + Environment.NewLine +
                        " --> URL: {avatar_url}",
                        DiscordLoggerUtilities.GetUsername(args.UserBefore),
                        args.UserBefore.Id,
                        args.UserAfter.AvatarUrl
                    );
                }
            }
            return Task.CompletedTask;
        }
    }
}
