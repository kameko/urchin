
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    public sealed class DiscordCommandConfigurationItem
    {
        public bool Enabled { get; set; }
        public string? Name { get; set; }
        public IEnumerable<string>? Authorized { get; set; }
        public IEnumerable<string>? AuthorBlacklist { get; set; }
    }
}
