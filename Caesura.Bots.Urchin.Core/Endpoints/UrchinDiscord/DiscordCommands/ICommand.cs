
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    
    public interface ICommand : IAsyncDisposable
    {
        IEnumerable<string> Aliases { get; }
        IDictionary<string, string>? Arguments { get; }
        string HelpString { get; }
        
        Task<bool> Run(DiscordClient client, MessageCreateEventArgs args1, string prefix);
    }
}
