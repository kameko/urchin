
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Configurations;
    using Common.Infrastructure;
    using Common.Extensions;
    using Common.Algorithms;
    using StandardCommands;
    
    // TODO: Some sort of builder/state machine for commands to use.
    // Just take a look at the RemoteEcho command and you can see the potential.
    
    public sealed class CommandProcessor : IAsyncDisposable
    {
        public string Prefix { get; private set; }
        
        public UrchinDiscordClient SystemDiscordClient => urchinDiscord;
        
        private UrchinDiscordClient urchinDiscord;
        private ILogger log;
        private ExceptionListener listener;
        private List<UrchinDiscordCommandConiguration>? cmdConfigs;
        private List<ulong> admins;
        private List<ICommand> commands;
        private List<string> command_names;
        
        private CancellationTokenSource? cts;
        private ConcurrentQueue<(DiscordClient, MessageCreateEventArgs)> workQueue;
        private Thread commandProcRuntimeThread;
        
        public CommandProcessor(
            UrchinDiscordClient urchinDiscordClient,
            ILoggerFactory logger,
            ExceptionListener exceptionListener,
            UrchinConfigurationRoot configuration
        )
        {
            Prefix = ".\\";
            
            urchinDiscord = urchinDiscordClient;
            log           = logger.CreateLogger<CommandProcessor>();
            listener      = exceptionListener;
            
            // NOTE: When creating a new command, make sure to add it's name to GetEmittable() in
            // UrchinConfiguration/UrchinDiscordConfiguration.cs
            commands = new()
            {
                new Shutdown(log, exceptionListener, urchinDiscordClient),
                new Echo(log, exceptionListener),
                new RemoteEcho(log, exceptionListener, this),
                new Scan(log, exceptionListener),
                new SetAvatar(log, exceptionListener),
                new SetActivity(log, exceptionListener, this),
                new Help(log, exceptionListener, this),
                new Source(log, exceptionListener),
            };
            command_names = new();
            RefreshCommandNames();
            
            cmdConfigs = configuration.Discord.Commands;
            admins     = configuration.Discord.Admins.Select(x => x.Id).ToList();
            
            workQueue = new();
            commandProcRuntimeThread = new(Runtime);
            commandProcRuntimeThread.Name = nameof(commandProcRuntimeThread);
        }
        
        public (ICommand? command, bool enabled) GetCommand(string name)
        {
            var cmd = commands.Find(x =>
                x.Aliases.Any(y =>
                    string.Equals(y, name, StringComparison.InvariantCultureIgnoreCase)
                )
            );
            if (cmd is null)
            {
                return (null, false);
            }
            
            var config = cmdConfigs?.Find(x =>
                cmd.Aliases.Any(y =>
                    string.Equals(x.Name, y, StringComparison.InvariantCultureIgnoreCase)
                )
            );
            
            return (cmd, config?.Enabled ?? false);
        }
        
        public IEnumerable<ICommand> GetCommands()
        {
            var list = new List<ICommand>(commands.Count);
            foreach (var command in commands)
            {
                var config = cmdConfigs?.Find(x =>
                    command.Aliases.Any(y =>
                        string.Equals(x.Name, y, StringComparison.InvariantCultureIgnoreCase)
                    )
                );
                if (config?.Enabled ?? false)
                {
                    list.Add(command);
                }
            }
            return list;
        }
        
        public bool IsUserAuthorized(ICommand command, ulong userId) => ValidateSecurity(command, userId);
        
        public bool IsUserAdmin(ulong userId) => admins.Contains(userId);
        
        public Task Start(CancellationToken token)
        {
            if (cmdConfigs is null || (cmdConfigs is not null && cmdConfigs.Count() == 0))
            {
                log.LogWarning("Could not find configuration for Discord commands. No commands will function.");
            }
            else if (cmdConfigs is not null)
            {
                var count = cmdConfigs.Count;
                if (count == 0)
                {
                    log.LogInformation("No Discord commands are enabled");
                }
                else if (count == 1)
                {
                    var command = cmdConfigs.First();
                    log.LogInformation("Enabled Discord commands: [ {command_name_1} ]", command.Name);
                }
                else
                {
                    var s = new StringBuilder();
                    var o = new List<string>();
                    
                    s.Append("Enabled Discord commands: [ ");
                    
                    for (var i = 0; i < count; i++)
                    {
                        var command = cmdConfigs[i];
                        if (command.Enabled && !string.IsNullOrWhiteSpace(command.Name))
                        {
                            s.Append("{command_name_");
                            s.Append(i);
                            s.Append("}");
                            o.Add(command.Name);
                            if (i != count - 1)
                            {
                                s.Append(", ");
                            }
                        }
                    }
                    
                    s.Append(" ]");
                    
                    log.LogInformation(s.ToString(), o.ToArray());
                }
                
                cts = CancellationTokenSource.CreateLinkedTokenSource(token);
                commandProcRuntimeThread.Start();
            }
            return Task.CompletedTask;
        }
        
        public Task Handle(DiscordClient client, MessageCreateEventArgs args)
        {
            workQueue.Enqueue((client, args));
            return Task.CompletedTask;
        }
        
        public async ValueTask DisposeAsync()
        {
            foreach (var command in commands)
            {
                await command.DisposeAsync();
            }
        }
        
        private void Runtime()
        {
            Task.Run(async () =>
            {
                if (cts is null)
                {
                    return;
                }
                var token = cts.Token;
                while (!token.IsCancellationRequested)
                {
                    var success = workQueue.TryDequeue(out var item);
                    if (success)
                    {
                        var (client, args) = item;
                        await HandleInternal(client, args);
                    }
                    await Task.Delay(16);
                }
            })
            .ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    listener.Tell(task.Exception!);
                }
            })
            .Wait();
        }
        
        private async Task HandleInternal(DiscordClient client, MessageCreateEventArgs args)
        {
            if (cmdConfigs is null || (cmdConfigs is not null && cmdConfigs.Count() == 0))
            {
                return;
            }
            
            var name = args.Message.Content.Split().FirstOrDefault()?.Replace(Prefix, "").ToLower();
            if (name is not null && !string.IsNullOrWhiteSpace(name))
            {
                var (command, enabled) = GetCommand(name);
                var notFoundMessage1   = $"Command not found: \"{name.ToUpper()}\".";
                var notFoundMessage2   = $"\nType `{Prefix}help list` for a list of all valid commands.";
                if (command is null)
                {
                    log.LogInformation(
                        "Discord user {user_name} ({user_ud}) attempted to invoke a command but command does not exist."
                        + Environment.NewLine + " --> {message}",
                        $"{args.Author.Username}#{args.Author.Discriminator}".RemoveControlChar(),
                        args.Author.Id,
                        args.Message.Content.RemoveControlChar()
                    );
                    
                    try
                    {
                        var got_suggestion = CommandNameSuggestion(name, out var suggestion);
                        if (got_suggestion)
                        {
                            await args.Message.RespondAsync(
                                notFoundMessage1 + $"\nDid you mean `{suggestion}`?" + notFoundMessage2
                            );
                        }
                        else
                        {
                            await args.Message.RespondAsync(notFoundMessage1 + notFoundMessage2);
                        }
                    }
                    catch (Exception e)
                    {
                        log.LogWarning(
                            "Attempt to respond to user attempting to invoke a non-existent command, " +
                            "but encountered an exception." + Environment.NewLine +
                            " --> {exception_message}",
                            e.Message
                        );
                    }
                    
                    return;
                }
                
                if (!enabled)
                {
                    log.LogInformation(
                        "Discord user {user_name} ({user_ud}) attempted to invoke a command but command is not enabled."
                        + Environment.NewLine + " --> {message}",
                        $"{args.Author.Username}#{args.Author.Discriminator}".RemoveControlChar(),
                        args.Author.Id,
                        args.Message.Content.RemoveControlChar()
                    );
                    
                    try
                    {
                        await args.Message.RespondAsync(notFoundMessage1 + notFoundMessage2);
                    }
                    catch (Exception e)
                    {
                        log.LogWarning(
                            "Attempt to respond to user attempting to invoke a disabled command, " +
                            "but encountered an exception." + Environment.NewLine +
                            " --> {exception_message}",
                            e.Message
                        );
                    }
                    
                    return;
                }
                
                var authorized = ValidateSecurity(command, args.Author.Id);
                if (authorized)
                {
                    log.LogInformation(
                        "Discord user {user_name} ({user_ud}) invoked a command."
                        + Environment.NewLine + " --> {message}",
                        $"{args.Author.Username}#{args.Author.Discriminator}".RemoveControlChar(),
                        args.Author.Id,
                        args.Message.Content.RemoveControlChar()
                    );
                    
                    try
                    {
                        await command.Run(client, args, Prefix);
                    }
                    catch (Exception e)
                    {
                        listener.Tell(e);
                    }
                }
                else
                {
                    log.LogInformation(
                        "Discord user {user_name} ({user_ud}) attempted to invoke a command but was unauthorized."
                        + Environment.NewLine + " --> {message}",
                        $"{args.Author.Username}#{args.Author.Discriminator}".RemoveControlChar(),
                        args.Author.Id,
                        args.Message.Content.RemoveControlChar()
                    );
                    
                    try
                    {
                        await args.Message.RespondAsync($"You are not authorized to run command \"{name.ToUpper()}\".");
                    }
                    catch (Exception e)
                    {
                        log.LogWarning(
                            "Attempt to respond to unathorized user attempting to invoke a command, " +
                            "but encountered an exception." + Environment.NewLine +
                            " --> {exception_message}",
                            e.Message
                        );
                    }
                }
            }
        }
        
        private bool CommandNameSuggestion(string name, out string? suggestion)
        {
            var leastDistance       = int.MaxValue;
            var nameOfLeastDistance = string.Empty;
            foreach (var cmdname in command_names)
            {
                
                if (name.Length > 1 &&
                    cmdname.StartsWith(name, StringComparison.InvariantCultureIgnoreCase) || 
                    name.StartsWith(cmdname, StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    suggestion = cmdname;
                    return true;
                }
                
                var distance = StringDistance.Levenshtein(cmdname, name);
                if (distance <= cmdname.Length / 2 && distance < leastDistance)
                {
                    leastDistance       = distance;
                    nameOfLeastDistance = cmdname;
                }
            }
            
            if (leastDistance != int.MaxValue && !string.IsNullOrEmpty(nameOfLeastDistance))
            {
                suggestion = nameOfLeastDistance;
                return true;
            }
            
            suggestion = null;
            return false;
        }
        
        private bool ValidateSecurity(ICommand command, ulong author)
        {
            if (IsUserAdmin(author))
            {
                return true;
            }
            
            var config = cmdConfigs?.Find(x =>
                command.Aliases.Any(y =>
                    string.Equals(x.Name, y, StringComparison.InvariantCultureIgnoreCase)
                )
            );
            if (config is not null)
            {
                if (config.Authorized is not null)
                {
                    if (config.Authorized.Exists(x => x.FriendlyName == "*"))
                    {
                        if (config.AuthorBlacklist is not null)
                        {
                            foreach (var (name, id) in config.AuthorBlacklist)
                            {
                                if (id == author)
                                {
                                    return false;
                                }
                            }
                        }
                        
                        return true;
                    }
                    else
                    {
                        foreach (var (name, id) in config.Authorized)
                        {
                            if (id == author)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        
        private void RefreshCommandNames()
        {
            command_names.Clear();
            foreach (var command in commands)
            {
                foreach (var name in command.Aliases)
                {
                    command_names.Add(name);
                }
            }
        }
    }
}
