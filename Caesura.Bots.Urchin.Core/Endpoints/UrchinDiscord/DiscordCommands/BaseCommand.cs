
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public abstract class BaseCommand : ICommand
    {
        public IEnumerable<string> Aliases { get; private set; }
        public IDictionary<string, string>? Arguments { get; protected set; }
        public string HelpString { get; protected set; }
        
        protected ILogger Log { get; private set; }
        protected ExceptionListener Listener { get; private set; }
        
        public BaseCommand(ILogger logger, ExceptionListener exceptionListener, IEnumerable<string> aliases)
        {
            Aliases  = aliases;
            Log      = logger;
            Listener = exceptionListener;
            
            HelpString = "No help document defined.";
        }
        
        public BaseCommand(ILogger logger, ExceptionListener exceptionListener, string name) :
            this(logger, exceptionListener, new List<string>() { name }) { }
        
        public async Task<bool> Run(DiscordClient client, MessageCreateEventArgs args, string prefix)
        {
            var tokens = args.Message.Content.Split().ToList();
            var name   = tokens.First().Replace(prefix, "");
            tokens.RemoveAt(0); // remove name
            return await Invoke(client, args, name, tokens);
        }
        
        protected abstract Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments);
        
        public virtual ValueTask DisposeAsync() => new ValueTask();
    }
}
