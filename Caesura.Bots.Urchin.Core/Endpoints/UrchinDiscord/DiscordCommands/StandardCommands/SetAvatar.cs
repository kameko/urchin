
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Net.Http;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    // TODO: enable using a message ID to fetch the image from.
    
    public sealed class SetAvatar : BaseCommand
    {
        private HttpClient? httpClient;
        
        public SetAvatar(ILogger logger, ExceptionListener exceptionListener) :
            base(logger, exceptionListener, new List<string>() { "setavatar" })
        {
            
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            if (args.Message.Attachments.Count == 0)
            {
                try
                {
                    await args.Message.RespondAsync("No image file attached to message. Cannot set avatar.");
                }
                catch
                {
                    // don't care
                }
                
                return true;
            }
            
            try
            {
                try
                {
                    if (httpClient is null)
                    {
                        var handler = new SocketsHttpHandler
                        {
                            // Sets how long a connection can be in the pool to be considered reusable (by default - infinite)
                            PooledConnectionLifetime = TimeSpan.FromMinutes(1),
                        };
                        httpClient = new HttpClient(handler, false);
                    }
                    
                    using var discord_stream = await httpClient.GetStreamAsync(args.Message.Attachments.First().Url);
                    var stream = new MemoryStream();
                    await discord_stream.CopyToAsync(stream);
                    await client.UpdateCurrentUserAsync(avatar: stream);
                    await args.Message.RespondAsync("Success");
                }
                catch (Exception e0)
                {
                    await args.Message.RespondAsync("Operation failed: " + e0.Message);
                }
            }
            catch (Exception e)
            {
                Log.LogWarning(e, "General problem attempting to set avatar");
            }
            
            return true;
        }
        
        public override ValueTask DisposeAsync()
        {
            httpClient?.Dispose();
            return new ValueTask();
        }
    }
}
