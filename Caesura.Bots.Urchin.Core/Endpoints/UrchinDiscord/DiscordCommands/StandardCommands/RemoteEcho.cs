
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Net.Http;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    // TODO: a parser to generate certain message aspects, such as mentioning someone/everyone.
    // should look like ".\rs CHANNELID {at:USERID} hello!"
    // Also make a "respond" alias that specifically uses a channel ID and a message ID
    
    public sealed class RemoteEcho : BaseCommand
    {
        private CommandProcessor cmdProc;
        private HttpClient? httpClient;
        
        public RemoteEcho(ILogger logger, ExceptionListener exceptionListener, CommandProcessor commandProcessor) :
            base(logger, exceptionListener, new List<string>() { "remoteecho", "remotesay", "rs", "re" })
        {
            cmdProc = commandProcessor;
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            try
            {
                var text = string.Empty;
                
                if (arguments.Count() < 2 && args.Message.Attachments.Count == 0)
                {
                    await args.Message.RespondAsync("[ERROR] Not enough arguments. Needs a channel ID and text to send.");
                    return false;
                }
                else if (arguments.Count() == 0 && args.Message.Attachments.Count > 0)
                {
                    await args.Message.RespondAsync("[ERROR] Not enough arguments. Needs at least a channel ID, and may include optional text.");
                    return false;
                }
                
                if (arguments.Count() > 1)
                {
                    text = string.Join(" ", arguments.TakeLast(arguments.Count() - 1));
                }
                
                var channel_id_success = ulong.TryParse(arguments.FirstOrDefault(), out var channel_id);
                
                if (channel_id_success)
                {
                    try
                    {
                        var target_channel = await client.GetChannelAsync(channel_id);
                        
                        if (!cmdProc.IsUserAdmin(args.Author.Id) && !target_channel.Users.Any(x => x.Id == args.Author.Id))
                        {
                            await args.Message.RespondAsync(
                                "ERROR: You must have access to the target channel to be able to send a message to it."
                            );
                            return true;
                        }
                        
                        try
                        {
                            if (args.Message.Attachments.Count == 0)
                            {
                                await target_channel.SendMessageAsync(text);
                            }
                            else
                            {
                                if (httpClient is null)
                                {
                                    var handler = new SocketsHttpHandler
                                    {
                                        // Sets how long a connection can be in the pool to be considered reusable (by default - infinite)
                                        PooledConnectionLifetime = TimeSpan.FromMinutes(1),
                                    };
                                    httpClient = new HttpClient(handler, false);
                                }
                                
                                using var discord_stream = await httpClient.GetStreamAsync(args.Message.Attachments.First().Url);
                                await target_channel.SendMessageAsync(builder =>
                                {
                                    builder.WithFile(
                                        args.Message.Attachments.First().FileName,
                                        discord_stream
                                    );
                                    if (!string.IsNullOrWhiteSpace(text))
                                    {
                                        builder.Content = text;
                                    }
                                });
                            }
                            
                            if (target_channel.Id != args.Message.Channel.Id)
                            {
                                await args.Message.RespondAsync("Successfully sent message.");
                            }
                        }
                        catch
                        {
                            await args.Message.RespondAsync(
                                $"[ERROR] Could not send message to channel with ID {arguments.FirstOrDefault()}.\n" +
                                "I may not be authorized to send messages to it."
                            );
                        }
                    }
                    catch
                    {
                        await args.Message.RespondAsync(
                            $"[ERROR] Could not fetch channel with ID {arguments.FirstOrDefault()}.\n" +
                            "It may not exist or I may be unauthorized to fetch it."
                        );
                    }
                }
                else
                {
                    await args.Message.RespondAsync($"[ERROR] Could not find channel with ID {arguments.FirstOrDefault()}.");
                }
            }
            catch (Exception e)
            {
                Log.LogWarning(
                    "Discord ECHO command encountered an exception. Ignoring." + Environment.NewLine +
                    " --> {exception_message}",
                    e.Message
                );
            }
            return true;
        }
        
        public override ValueTask DisposeAsync()
        {
            httpClient?.Dispose();
            return new ValueTask();
        }
    }
}
