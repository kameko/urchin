
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public sealed class SetActivity : BaseCommand
    {
        private CommandProcessor cmdProc;
        
        public SetActivity(ILogger logger, ExceptionListener exceptionListener, CommandProcessor commandProcessor) :
            base(logger, exceptionListener, new List<string>() { "setactivity" })
        {
            cmdProc = commandProcessor;
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            try
            {
                try
                {
                    await cmdProc.SystemDiscordClient.SetActivity(new DiscordActivity(string.Join(" ", arguments)));
                    await args.Message.RespondAsync("Success");
                }
                catch (Exception e0)
                {
                    await args.Message.RespondAsync("Operation failed: " + e0.Message);
                }
            }
            catch (Exception e)
            {
                Log.LogWarning(e, "General problem attempting to set activity");
            }
            
            return true;
        }
    }
}
