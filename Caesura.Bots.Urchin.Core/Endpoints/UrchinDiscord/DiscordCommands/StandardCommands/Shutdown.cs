
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public sealed class Shutdown : BaseCommand
    {
        private UrchinDiscordClient urchinClient;
        
        public Shutdown(ILogger logger, ExceptionListener exceptionListener, UrchinDiscordClient udc) :
            base(logger, exceptionListener, new List<string>() { "shutdown" })
        {
            urchinClient = udc;
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            var text = string.Join(" ", arguments);
            try
            {
                await args.Channel.SendMessageAsync("Shutting down...");
            }
            catch (Exception e)
            {
                Log.LogWarning(
                    "Discord SHUTDOWN command encountered an exception. Ignoring." + Environment.NewLine +
                    " --> {exception_message}",
                    e.Message
                );
            }
            finally
            {
                // TODO: check for message
                // Flags to check: "-r", "--reason", "-m", "--message"
                urchinClient.InvokeShutdownRequest("SHUTDOWN command.");
            }
            return true;
        }
    }
}
