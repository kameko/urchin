
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public sealed class Source : BaseCommand
    {
        public Source(ILogger logger, ExceptionListener exceptionListener) :
            base(logger, exceptionListener, new List<string>() { "source", "src", "repo", "gitlab", "github" })
        {
            
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            try
            {
                var src = "https://gitlab.com/kameko/urchin";
                
                var embed = new DiscordEmbedBuilder();
                embed.Title = "Urchin Bot Source Code Repository";
                embed.Url   = src;
                embed.AddField("URL", src);
                
                await args.Message.RespondAsync(embed);
                
                if (string.Equals(name, "github", StringComparison.InvariantCultureIgnoreCase))
                {
                    await Task.Delay(TimeSpan.FromSeconds(3));
                    await args.Message.RespondAsync("Also GitHub is for suckers, how dare you imply that I use it.");
                }
            }
            catch (Exception e)
            {
                Log.LogError(e, "Discord Source Command");
            }
            
            return true;
        }
    }
}
