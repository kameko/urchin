
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Net.Http;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public sealed class Echo : BaseCommand
    {
        private HttpClient? httpClient;
        
        public Echo(ILogger logger, ExceptionListener exceptionListener) :
            base(logger, exceptionListener, new List<string>() { "echo", "say" })
        {
            
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            Stream? stream = null;
            try
            {
                var text = string.Join(" ", arguments);
                
                if (string.IsNullOrWhiteSpace(text) && args.Message.Attachments.Count == 0)
                {
                    await args.Message.RespondAsync("Please provide text and/or an attachment.");
                    return true;
                }
                
                if (args.Message.Attachments.Count > 0)
                {
                    if (httpClient is null)
                    {
                        var handler = new SocketsHttpHandler
                        {
                            // Sets how long a connection can be in the pool to be considered reusable (by default - infinite)
                            PooledConnectionLifetime = TimeSpan.FromMinutes(1),
                        };
                        httpClient = new HttpClient(handler, false);
                    }
                    
                    stream = await httpClient.GetStreamAsync(args.Message.Attachments.First().Url);
                }
                await args.Channel.SendMessageAsync(builder =>
                {
                    if (stream is not null)
                    {
                        builder.WithFile(
                            args.Message.Attachments.First().FileName,
                            stream
                        );
                    }
                    
                    if (!string.IsNullOrWhiteSpace(text))
                    {
                        builder.Content = text;
                    }
                });
            }
            catch (Exception e)
            {
                Log.LogWarning(
                    "Discord ECHO command encountered an exception. Ignoring." + Environment.NewLine +
                    " --> {exception_message}",
                    e.Message
                );
            }
            finally
            {
                if (stream is not null)
                {
                    await stream.DisposeAsync();
                }
            }
            return true;
        }
        
        public override ValueTask DisposeAsync()
        {
            httpClient?.Dispose();
            return new ValueTask();
        }
    }
}
