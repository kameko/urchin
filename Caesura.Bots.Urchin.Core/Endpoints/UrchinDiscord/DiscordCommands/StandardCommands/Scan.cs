
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public sealed class Scan : BaseCommand
    {
        public Scan(ILogger logger, ExceptionListener exceptionListener) :
            base(logger, exceptionListener, new List<string>() { "scan" })
        {
            
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            await args.Message.RespondAsync("Not implemented");
            
            // TODO:
            
            return true;
        }
    }
}
