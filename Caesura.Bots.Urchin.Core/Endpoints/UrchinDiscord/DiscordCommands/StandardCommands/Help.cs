
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordCommands.StandardCommands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Infrastructure;
    
    public sealed class Help : BaseCommand
    {
        private CommandProcessor cmdProc;
        private DiscordEmbed? listEmbed;
        
        public Help(ILogger logger, ExceptionListener exceptionListener, CommandProcessor commandProcessor) :
            base(logger, exceptionListener, new List<string>() { "help", "list" })
        {
            cmdProc = commandProcessor;
        }
        
        protected override async Task<bool> Invoke(DiscordClient client, MessageCreateEventArgs args, string name, IEnumerable<string> arguments)
        {
            try
            {
                var prefix = cmdProc.Prefix;
                
                if (arguments.Count() == 0 && !string.Equals(name, "list", StringComparison.InvariantCultureIgnoreCase))
                {
                    await args.Message.RespondAsync(
                        $"Type `{prefix}help COMMAND_NAME_HERE` for help with a specific command, " +
                        $"or type `{prefix}help list` for a list of commands."
                    );
                    return true;
                }
                
                var command_name = arguments.FirstOrDefault();
                
                if (
                    string.Equals(command_name , "list", StringComparison.InvariantCultureIgnoreCase) ||
                    string.Equals(name         , "list", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    var commands = cmdProc.GetCommands();
                    
                    if (listEmbed is null)
                    {
                        var sb      = new StringBuilder();
                        var embed   = new DiscordEmbedBuilder();
                        embed.Title = "Commands and aliases";
                        
                        // maybe todo---: add a help file in the repo and link to it here
                        
                        foreach (var x in commands)
                        {
                            sb.Clear();
                            var cmd   = x.Aliases.First();
                            var count = x.Aliases.Count();
                            if (count == 1)
                            {
                                embed.AddField(cmd, cmd, inline: true);
                            }
                            else
                            {
                                for (var i = 0; i < count; i++)
                                {
                                    sb.Append(x.Aliases.ElementAt(i));
                                    if (i != count - 1)
                                    {
                                        sb.Append(", ");
                                    }
                                }
                                embed.AddField(cmd, sb.ToString(), inline: true);
                            }
                        }
                        
                        listEmbed = embed.Build();
                    }
                    
                    var unauthCmds = new List<ICommand>();
                    foreach (var x in commands)
                    {
                        if (!cmdProc.IsUserAuthorized(x, args.Author.Id))
                        {
                            unauthCmds.Add(x);
                        }
                    }
                    
                    if (unauthCmds.Count > 0)
                    {
                        var sb = new StringBuilder();
                        var count = unauthCmds.Count;
                        for (var i = 0; i < count; i++)
                        {
                            sb.Append(unauthCmds[i]);
                            if (i != count - 1)
                            {
                                sb.Append(", ");
                            }
                        }
                        
                        var embed = new DiscordEmbedBuilder(listEmbed);
                        var user  = $"{args.Author.Username}#{args.Author.Discriminator}";
                        embed.AddField($"You ({user}) are not authorized to execute the following commands", sb.ToString());
                        
                        await args.Message.RespondAsync(embed);
                    }
                    else
                    {
                        await args.Message.RespondAsync(listEmbed);
                    }
                    
                    return true;
                }
                
                var (command, enabled) = cmdProc.GetCommand(command_name!);
                
                if (command is null || !enabled)
                {
                    await args.Message.RespondAsync($"Help Error: Command \"{command_name!.ToUpper()}\" does not exist.");
                    return true;
                }
                
                var helpstr      = command.HelpString;
                var cmdArguments = command.Arguments;
                var authorized   = cmdProc.IsUserAuthorized(command, args.Author.Id);
                
                var cmdEmbed = new DiscordEmbedBuilder();
                
                cmdEmbed.AddField("Information", helpstr);
                if (cmdArguments is not null && cmdArguments.Count > 0)
                {
                    foreach (var (argName, argDoc) in cmdArguments)
                    {
                        cmdEmbed.AddField("Argument " + argName, argDoc);
                    }
                }
                if (!authorized)
                {
                    cmdEmbed.AddField("Notice", "You are not authorized to execute this command.");
                }
                
                await args.Message.RespondAsync(cmdEmbed);
            }
            catch (Exception e)
            {
                Log.LogError(e, "Discord Help Command");
            }
            
            return true;
        }
    }
}
