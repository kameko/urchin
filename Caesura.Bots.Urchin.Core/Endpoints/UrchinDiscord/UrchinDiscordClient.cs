
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Configurations;
    using Common.Infrastructure;
    using Common.Extensions;
    using AI.S2K3.Classic;
    using AI.S2K3.Classic.Entities;
    using Services;
    using DiscordLogEvents;
    using DiscordCommands;
    using DiscordServices;
    using DiscordServices.StandardServices;
    
    // TODO: write activity parser (like replace {version} with the actual version)
    // This will require getting the system version as described in Program.cs
    // TODO: feed txt files into the AI.
    // TODO: Have a global channel blacklist that excludes all interactions,
    // including commands and even logging. The only interaction is if the
    // command prefix is invoked, then the user is informed that the current
    // channel is explicitly prohibited from all observation and interaction.
    // Also work with users, essentially blocking them.
    
    // https://dsharpplus.github.io/articles/advanced_topics/buttons.html 
    
    public sealed class UrchinDiscordClient : IRemoteClient
    {
        public string Name => "Discord";
        public bool Running => running;
        public event Action<IRemoteClient, string>? OnRequestShutdown;
        
        public string CommandPrefix { get; private set; }
        
        private bool running;
        private ILogger log;
        private UrchinConfigurationRoot config;
        private ExceptionListener listener;
        private LoggingDiscordEventHandlers logHandlers;
        private DiscordServiceContainer discordServices;
        private CommandProcessor commands;
        private CancellationTokenSource? cts;
        private DiscordClient client;
        private DiscordActivity activity;
        private UserStatus status;
        
        private MarkovResponder responder;
        
        public UrchinDiscordClient(
            ILoggerFactory loggerFactory,
            UrchinConfigurationRoot configuration,
            ExceptionListener exceptionListener,
            ServiceContainer systemServices,
            MarkovResponder markovResponder
        )
        {
            running         = false;
            log             = loggerFactory.CreateLogger<UrchinDiscordClient>();
            config          = configuration;
            listener        = exceptionListener;
            logHandlers     = new(exceptionListener, log);
            commands        = new(this, loggerFactory, exceptionListener, config);
            discordServices = new(loggerFactory, configuration, exceptionListener, systemServices);
            
            systemServices.Add(discordServices);
            
            // NOTE: When creating a new service, make sure to add it's configuration in
            // UrchinConfiguration/UrchinDiscordConfiguration.cs
            discordServices.Add(new AttachmentDownloader(loggerFactory, configuration));
            
            activity = new DiscordActivity();
            status   = UserStatus.Online;
            
            if (!string.IsNullOrWhiteSpace(config.Discord.Status))
            {
                activity = new DiscordActivity(config.Discord.Status);
            }
            
            responder = markovResponder;
            
            client        = SetupClient(loggerFactory);
            CommandPrefix = config.Discord.CommandPrefix;
        }
        
        // --- METHODS --- //
        
        public async Task Reconnect()
        {
            await client.DisconnectAsync();
            await client.ConnectAsync(activity, status);
        }
        
        public Task SendMessage(DiscordChannel channel, Action<DiscordMessageBuilder> action)
        {
            // maybe todo - cache message so we know to wait and see if it got received by us.
            return client.SendMessageAsync(channel, action);
        }
        
        public async Task SetActivity(DiscordActivity new_activity)
        {
            await client.UpdateStatusAsync(new_activity);
            activity = new_activity;
        }
        
        public async Task SetStatus(UserStatus new_status)
        {
            await client.UpdateStatusAsync(userStatus: new_status);
            status = new_status;
        }
        
        // --- INTERFACE IMPLEMENTATIONS --- //
        
        public Task AutoStart(CancellationToken token)
        {
            if (config.Discord.AutoStart)
            {
                return Start(token);
            }
            
            return Task.CompletedTask;
        }
        
        public async Task Start(CancellationToken token)
        {
            log.LogInformation("Starting Discord service...");
            
            if (running)
            {
                throw new InvalidOperationException("Client is already running.");
            }
            running = true;
            
            cts   = CancellationTokenSource.CreateLinkedTokenSource(token);
            token = cts.Token;
            
            if (client is null)
            {
                throw new InvalidOperationException("Discord client was not set up properly.");
            }
            if (token.IsCancellationRequested)
            {
                log.LogWarning("Cannot start Discord service, cancellation requested on start.");
            }
            
            try
            {
                log.LogInformation("Discord Intents: {intents}", client.Intents);
                await client.ConnectAsync(activity, status);
            }
            catch
            {
                log.LogError("Discord service did not start successfully.");
                throw;
            }
            
            log.LogInformation("Discord service started successfully.");
        }
        
        public async ValueTask DisposeAsync()
        {
            cts?.Cancel();
            await commands.DisposeAsync();
            await discordServices.DisposeAsync();
            if (client is not null)
            {
                await client.DisconnectAsync();
                client.Dispose();
            }
        }
        
        // --- UTILITIES --- //
        
        private DiscordClient SetupClient(ILoggerFactory loggerFactory)
        {
            var token = config.Discord.Token;
            if (string.IsNullOrWhiteSpace(token))
            {
                return null!;
            }
            
            var intents = config.Discord.AllIntents ? DiscordIntents.All : DiscordIntents.AllUnprivileged;
            
            client = new DiscordClient(new DiscordConfiguration()
            {
                Token         = token,
                LoggerFactory = loggerFactory,
                Intents       = intents,
            });
            
            logHandlers.HookAll(client);
            SetupEventHandlers(client);
            
            return client;
        }
        
        private void SetupEventHandlers(DiscordClient client)
        {
            client.Heartbeated    += (c, x) => Task.CompletedTask;
            client.Ready          += ClientReadyCallback;
            client.Resumed        += ClientResumedCallback;
            client.ClientErrored  += ClientErrorCallback;
            client.MessageCreated += MessageReceivedCallbackMeta;
            client.MessageUpdated += (c, x) => Task.CompletedTask;
        }
        
        // --- DISCORD CLIENT EVENT HANDLERS AND THEIR UTILITIES --- //
        
        private async Task ClientReadyCallback(DiscordClient client, ReadyEventArgs args)
        {
            log.LogInformation(
                "Logging into Discord as {user_name} ({user_id})",
                $"{client.CurrentUser.Username}#{client.CurrentUser.Discriminator}".RemoveControlChar(),
                client.CurrentUser.Id
            );
            
            var token = cts!.Token;
            await discordServices.Start(client, token);
            await commands.Start(token);
            
            await client.UpdateStatusAsync(new DiscordActivity("Urchin v0.1"));
        }
        
        private async Task ClientResumedCallback(DiscordClient client, ReadyEventArgs args)
        {
            // Sometimes Discord/DSharpPlus forgets our status after losing connection,
            // set them here to ensure they're always kept.
            await client.UpdateStatusAsync(activity);
            await client.UpdateStatusAsync(userStatus: status);
        }
        
        private async Task ClientErrorCallback(DiscordClient _, ClientErrorEventArgs args)
        {
            listener.Tell(args.Exception);
            await client.DisconnectAsync();
        }
        
        private Task MessageReceivedCallbackMeta(DiscordClient _, MessageCreateEventArgs args)
        {
            if (args.Author == client.CurrentUser)
            {
                // maybe todo - confirm that the message we wanted to send got sent.
                return Task.CompletedTask;
            }
            else
            {
                // This is to get around a bug in DSharpPlus.
                // See DiscordLogEvents/LoggingDiscordEventHandler.cs#Run for more information.
                return Task.Run(async () =>
                {
                    await MessageReceivedCallback(args);
                })
                .ContinueWith(async task =>
                {
                    if (task.IsFaulted)
                    {
                        listener.Tell(task.Exception!);
                        await client.DisconnectAsync();
                    }
                });
            }
        }
        
        private async Task MessageReceivedCallback(MessageCreateEventArgs args)
        {
            var source = args.Message.Channel.Id.ToString();
            if (args.Message.Channel.IsPrivate)
            {
                source = "Direct Message";
            }
            var markovMessage = new MarkovMessage()
            {
                TimeStamp = args.Message.CreationTimestamp,
                Source    = source,
                Sender    = args.Message.Author.Id.ToString(),
                Message   = args.Message.Content,
            };
            
            responder.NotifyMessage(markovMessage);
            
            if (args.Message.Content.StartsWith(commands.Prefix))
            {
                await commands.Handle(client, args);
            }
            else
            {
                await responder.PumpMessage(
                    markovMessage,
                    async response =>
                    {
                        var builder = new DiscordMessageBuilder();
                        builder.Content = response;
                        
                        await SendMessage(args.Channel, builder);
                    }
                );
            }
        }
        
        private async Task SendMessage(DiscordChannel channel, DiscordMessageBuilder builder)
        {
            await logHandlers.SendingMessage(client, channel, builder);
            await channel.SendMessageAsync(builder);
        }
        
        internal void InvokeShutdownRequest(string reason)
        {
            OnRequestShutdown?.Invoke(this, reason);
        }
    }
}
