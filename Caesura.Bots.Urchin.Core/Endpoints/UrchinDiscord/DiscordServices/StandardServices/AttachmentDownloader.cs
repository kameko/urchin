
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordServices.StandardServices
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text.Json;
    using System.IO;
    using System.Net.Http;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using DSharpPlus.Entities;
    using DSharpPlus.EventArgs;
    using Common.Configurations;
    
    // TODO: abstract file/directory IO by using Common.Abstractions and properly test this
    // TODO: abstract this in general and make it multi-endpoint, it only works with other
    // services, and put it in the urchin services folder, not the discord one. Get rid of
    // discord services entirely.
    // TODO: also get user avatar/banner
    
    public sealed class AttachmentDownloader : IDiscordService
    {
        public string Name => nameof(AttachmentDownloader);
        public bool Enabled { get; private set; }
        public bool Pulsable => true;
        public bool Started { get; private set; }
        
        private ILogger log;
        private ConcurrentQueue<MessageCreateEventArgs> workQueue;
        private HttpClient? httpClient;
        
        private DateTimeOffset lastFolderCheck;
        private bool autoDelete;
        private string savePath;
        private int oldestFolderTimeout;
        private int deleteCheckHourInterval;
        private bool channelsIsWhitelist;
        private List<ulong> channels;
        
        public AttachmentDownloader(ILoggerFactory logger, UrchinConfigurationRoot config)
        {
            Enabled   = config.Discord.Services.AttachmentDownloader.Enabled;
            log       = logger.CreateLogger<AttachmentDownloader>();
            workQueue = new();
            
            lastFolderCheck         = DateTime.UtcNow;
            autoDelete              = config.Discord.Services.AttachmentDownloader.AutoDelete;
            oldestFolderTimeout     = config.Discord.Services.AttachmentDownloader.DeleteOldestFolderAfterDays;
            deleteCheckHourInterval = config.Discord.Services.AttachmentDownloader.DeleteCheckHourInterval;
            savePath                = config.Discord.Services.AttachmentDownloader.SavePath;
            channelsIsWhitelist     = config.Discord.Services.AttachmentDownloader.ChannelsIsWhitelist;
            channels                = config.Discord.Services.AttachmentDownloader.Channels.Select(x => x.Id).ToList();
        }
        
        public Task Start(DiscordClient client, CancellationToken token)
        {
            Started = true;
            log.LogInformation("Saving Discord attachments to {save_path}", savePath);
            
            var handler = new SocketsHttpHandler
            {
                // Sets how long a connection can be in the pool to be considered reusable (by default - infinite)
                PooledConnectionLifetime = TimeSpan.FromMinutes(1),
            };
            httpClient = new(handler, false);
            
            client.MessageCreated += MessageCreated;
            
            return Task.CompletedTask;
        }
        
        public async Task Pulse(CancellationToken token)
        {
            var success = workQueue.TryDequeue(out var args);
            if (success)
            {
                try
                {
                    await FetchAttachment(args!);
                }
                catch (Exception e)
                {
                    log.LogWarning(e, "Problem attempting to fetch attachment");
                }
            }
            if (autoDelete && lastFolderCheck.AddHours(deleteCheckHourInterval) < DateTime.UtcNow)
            {
                lastFolderCheck = DateTime.UtcNow;
                try
                {
                    CheckFilesToDelete();
                }
                catch (Exception e)
                {
                    log.LogWarning(e, "Problem trying to handle old attachment deletion.");
                }
            }
        }
        
        public ValueTask DisposeAsync()
        {
            httpClient?.Dispose();
            return new ValueTask();
        }
        
        private async Task FetchAttachment(MessageCreateEventArgs args)
        {
            var contains = channels.Contains(args.Channel.Id);
            if (channelsIsWhitelist && !contains)
            {
                log.LogDebug("Channel not whitelisted, ignoring attachment.");
                return;
            }
            if (!channelsIsWhitelist && contains)
            {
                log.LogDebug("Channel is blacklisted, ignoring attachment.");
                return;
            }
            
            foreach (var attachment in args.Message.Attachments)
            {
                log.LogInformation("Saving {mime} attachment ({id}) \"{url}\"", attachment.MediaType, attachment.Id, attachment.Url);
                
                var guild     = args.Channel.IsPrivate ? "Direct Message" : args.Guild.Id.ToString();
                var date      = DateTime.UtcNow.ToString("dd-MM-yyyy");
                var dir       = Directory.CreateDirectory(Path.Combine(savePath, guild, args.Channel.Id.ToString(), date));
                var extension = Path.GetExtension(attachment.FileName);
                var path      = $"{Path.Combine(dir.FullName, attachment.Id.ToString())}{extension}";
                
                using var discordStream = await httpClient!.GetStreamAsync(attachment.Url);
                using var fileStream = new FileStream(path, FileMode.Create);
                await discordStream.CopyToAsync(fileStream);
                
                MapAttachmentIdToFilename(args.Message, attachment, args.Channel, dir);
                // MapChannelIdToFriendlyName(args.Guild, args.Channel, args.Author);
            }
        }
        
        private void CheckFilesToDelete()
        {
            if (!Directory.Exists(savePath))
            {
                return;
            }
            
            var guild_dirs = Directory.GetDirectories(savePath);
            foreach (var guild_dir in guild_dirs)
            {
                var channel_dirs = Directory.GetDirectories(guild_dir);
                foreach (var channel_dir in channel_dirs)
                {
                    var date_dirs = Directory.GetDirectories(channel_dir);
                    foreach (var date_dir in date_dirs)
                    {
                        var creation = Directory.GetCreationTimeUtc(date_dir);
                        var expired  = creation.AddDays(oldestFolderTimeout) < DateTime.UtcNow;
                        if (expired)
                        {
                            log.LogInformation("Discord attachment directory expired. Deleting. \"{dir}\"", date_dir);
                            var dir = new FileInfo(date_dir).Directory;
                            if (dir is not null)
                            {
                                dir.Delete(true);
                            }
                            else
                            {
                                log.LogInformation("Could not delete directory \"{dir}\"", channel_dir);
                            }
                        }
                    }
                }
            }
        }
        
        private void MapAttachmentIdToFilename(DiscordMessage message, DiscordAttachment attachment, DiscordChannel channel, DirectoryInfo directory)
        {
            using var writer = new StreamWriter(Path.Combine(directory.FullName, "map.txt"), append: true);
            writer.WriteLine($"(Message ID {message.Id}) {attachment.Id} --> {attachment.FileName}");
        }
        
        private void MapChannelIdToFriendlyName(DiscordGuild guild, DiscordChannel channel, DiscordUser author)
        {
            using var writer = new StreamWriter(Path.Combine(savePath, "map.txt"), append: true);
            if (channel.IsPrivate)
            {
                writer.WriteLine(
                    $"Direct Message/{channel.Id} --> Direct Message with " +
                    $"{author.Username}#{author.Discriminator} ({author.Id})"
                );
            }
            else
            {
                writer.WriteLine(
                    $"{guild.Id}/{channel.Id} --> {guild.Name}/#{channel.Name}"
                );
            }
        }
        
        private Task MessageCreated(DiscordClient client, MessageCreateEventArgs args)
        {
            workQueue.Enqueue(args);
            return Task.CompletedTask;
        }
    }
}
