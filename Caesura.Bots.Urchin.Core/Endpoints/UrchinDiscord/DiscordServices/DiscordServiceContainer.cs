
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using DSharpPlus;
    using Common.Configurations;
    using Common.Infrastructure;
    using Services;
    
    public sealed class DiscordServiceContainer : IService
    {
        public string Name => nameof(DiscordServiceContainer);
        public bool Enabled { get; private set; }
        public bool Pulsable => true;
        public bool Started { get; private set; }
        
        private ILogger log;
        private UrchinConfigurationRoot configRoot;
        private ExceptionListener listener;
        private List<IDiscordService> services;
        
        public DiscordServiceContainer(
            ILoggerFactory logger,
            UrchinConfigurationRoot config,
            ExceptionListener exceptionListener,
            ServiceContainer systemServices
        )
        {
            Enabled    = config.System.Services.DiscordServiceContainer.Enabled;
            log        = logger.CreateLogger<DiscordServiceContainer>();
            configRoot = config;
            listener   = exceptionListener;
            services   = new();
        }
        
        public void Add(IDiscordService service)
        {
            services.Add(service);
        }
        
        public async Task Start(DiscordClient client, CancellationToken token)
        {
            if (!Started)
            {
                return;
            }
            
            foreach (var service in services)
            {
                if (!service.Enabled)
                {
                    continue;
                }
                
                log.LogInformation("Starting Discord service {service_name}", service.Name);
                try
                {
                    await service.Start(client, token);
                }
                catch (Exception e)
                {
                    listener.Tell(e);
                }
            }
        }
        
        public Task Start(CancellationToken token)
        {
            Started = true;
            return Task.CompletedTask;
        }
        
        public async ValueTask DisposeAsync()
        {
            foreach (var service in services)
            {
                await service.DisposeAsync();
            }
        }
        
        public async Task Pulse(CancellationToken token)
        {
            try
            {
                foreach (var service in services)
                {
                    if (service.Pulsable)
                    {
                        await service.Pulse(token);
                    }
                }
            }
            catch (Exception e)
            {
                listener.Tell(e);
            }
        }
    }
}
