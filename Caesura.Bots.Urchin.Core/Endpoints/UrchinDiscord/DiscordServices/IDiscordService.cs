
namespace Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.DiscordServices
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using DSharpPlus;
    
    public interface IDiscordService : IAsyncDisposable
    {
        string Name { get; }
        bool Enabled { get; }
        bool Pulsable { get; }
        bool Started { get; }
        
        Task Start(DiscordClient client, CancellationToken token);
        Task Pulse(CancellationToken token);
    }
}
