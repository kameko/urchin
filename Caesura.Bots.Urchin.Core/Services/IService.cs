
namespace Caesura.Bots.Urchin.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface IService : IAsyncDisposable
    {
        string Name { get; }
        bool Enabled { get; }
        bool Pulsable { get; }
        bool Started { get; }
        
        Task Start(CancellationToken token);
        Task Pulse(CancellationToken token);
    }
}
