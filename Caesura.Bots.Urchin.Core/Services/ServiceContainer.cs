
namespace Caesura.Bots.Urchin.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Common.Configurations;
    using Common.Infrastructure;
    
    public sealed class ServiceContainer : IAsyncDisposable
    {
        private ILogger log;
        private UrchinConfigurationRoot configRoot;
        private ExceptionListener listener;
        private List<IService> services;
        private CancellationTokenSource? cts;
        private Thread runtimeThread;
        
        public ServiceContainer(ILoggerFactory logger, UrchinConfigurationRoot config, ExceptionListener exceptionListener)
        {
            log           = logger.CreateLogger<ServiceContainer>();
            configRoot    = config;
            listener      = exceptionListener;
            services      = new();
            runtimeThread = new(Runtime);
            runtimeThread.Name = nameof(ServiceContainer);
            runtimeThread.IsBackground = true;
        }
        
        public void Add(IService service)
        {
            services.Add(service);
        }
        
        public async Task Start(CancellationToken token)
        {
            cts   = CancellationTokenSource.CreateLinkedTokenSource(token);
            token = cts.Token;
            var pulsable = false;
            foreach (var service in services)
            {
                if (!service.Enabled)
                {
                    continue;
                }
                
                log.LogInformation("Starting service {service_name}", service.Name);
                await service.Start(token);
                if (service.Pulsable)
                {
                    pulsable = true;
                }
            }
            if (pulsable)
            {
                runtimeThread.Start();
            }
        }
        
        public async ValueTask DisposeAsync()
        {
            foreach (var service in services)
            {
                await service.DisposeAsync();
            }
        }
        
        private void Runtime()
        {
            Task.Run(async () =>
            {
                if (cts is null)
                {
                    return;
                }
                var token = cts.Token;
                while (!token.IsCancellationRequested)
                {
                    foreach (var service in services)
                    {
                        if (service.Started && service.Pulsable)
                        {
                            await service.Pulse(token);
                        }
                    }
                    
                    await Task.Delay(16);
                }
            })
            .ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    listener.Tell(task.Exception!);
                }
            })
            .Wait();
        }
    }
}
