
namespace Caesura.Bots.Urchin.Core.Services.UrchinTcp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Net;
    using System.Net.Sockets;
    using Microsoft.Extensions.Logging;
    using Common.Configurations;
    using Common.Infrastructure;
    using Common.Extensions;
    
    public sealed class UrchinTcpService : IService
    {
        public string Name => nameof(UrchinTcpService);
        public bool Enabled { get; private set; }
        public bool Pulsable => true;
        public bool Started { get; private set; }
        public event Action<IService, string>? OnRequestShutdown;
        
        private ILogger log;
        private UrchinConfigurationRoot configRoot;
        private ExceptionListener listener;
        
        private TcpListener tcp;
        private Task? tcpServerTask;
        private List<TcpConnection> connections;
        private List<Task> tcpClientTasks;
        private readonly object connections_lock = new object();
        
        private CancellationTokenSource? cts;
        
        public UrchinTcpService(
            ILoggerFactory logFactory,
            UrchinConfigurationRoot config,
            ExceptionListener exceptionListener
        )
        {
            log        = logFactory.CreateLogger<UrchinTcpService>();
            configRoot = config;
            listener   = exceptionListener;
            
            // TODO: get ip and port from config.
            // also if the IP is "localhost", change it to "127.0.0.1" because
            // "localhost" doesn't parse.
            var ip = IPAddress.Parse("127.0.0.1");
            tcp    = new TcpListener(ip, 20_001);
            
            connections    = new();
            tcpClientTasks = new();
        }
        
        public Task Start(CancellationToken token)
        {
            cts = CancellationTokenSource.CreateLinkedTokenSource(token);
            tcpServerTask = Task.Run(TcpServerRuntime);
            return Task.CompletedTask;
        }
        
        public Task Pulse(CancellationToken token)
        {
            return TcpClientHandler(token);
        }
        
        public void RequestShutdown()
        {
            OnRequestShutdown?.Invoke(this, "Make C# compiler shut up");
        }
        
        public ValueTask DisposeAsync()
        {
            foreach (var connection in connections)
            {
                try
                {
                    connection.Dispose();
                }
                catch (Exception e)
                {
                    log.LogWarning(e, "Connection threw an exception when disposing");
                }
            }
            try
            {
                tcp.Stop();
            }
            catch (Exception e)
            {
                log.LogWarning(e, "TCP server threw an exception when disposing");
            }
            return new ValueTask();
        }
        
        private void EstablishSession(TcpClient client)
        {
            lock (connections_lock)
            {
                var connection = new TcpConnection(client);
                connections.Add(connection);
            }
        }
        
        private async Task TcpServerRuntime()
        {
            if (cts is null)
            {
                return;
            }
            
            tcp.Start();
            
            var token = cts.Token;
            while (!token.IsCancellationRequested)
            {
                try
                {
                    var client = await tcp.AcceptTcpClientAsync().WithWaitCancellation(token);
                    EstablishSession(client);
                }
                catch (OperationCanceledException)
                {
                    // ignore
                }
                catch (SocketException se)
                {
                    log.LogWarning(se, "Socket Exception encountered in TCP server");
                }
                catch (Exception e)
                {
                    listener.Tell(e);
                }
                await Task.Delay(16);
            }
        }
        
        private async Task TcpClientHandler(CancellationToken token)
        {
            try
            {
                tcpClientTasks.Clear();
                
                lock (connections_lock)
                {
                    foreach (var connection in connections)
                    {
                        if (token.IsCancellationRequested)
                        {
                            tcpClientTasks.Clear();
                            return;
                        }
                        if (connection.Pulsable)
                        {
                            var task = connection.Pulse(token);
                            tcpClientTasks.Add(task);
                        }
                    }
                }
                
                if (tcpClientTasks.Count == 1)
                {
                    await tcpClientTasks.First();
                }
                else if (tcpClientTasks.Count > 0)
                {
                    await Task.WhenAll(tcpClientTasks);
                }
            }
            catch (SocketException se)
            {
                log.LogWarning(se, "SocketException encountered in TCP connection handler");
            }
        }
    }
}
