
namespace Caesura.Bots.Urchin.Core.Services.UrchinTcp
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text;
    using System.Net;
    using System.Net.Sockets;
    using Microsoft.Extensions.Logging;
    
    // TODO: move this along with the message types and login logic to Caesura.Bots.Urchin.Common.Networking
    // https://codinginfinite.com/multi-threaded-tcp-server-core-example-csharp/ 
    // https://www.codeproject.com/Articles/5270779/High-Performance-TCP-Client-Server-using-TCPListen 
    // https://riptutorial.com/csharp/example/4409/basic-tcp-communication-client 
    // https://github.com/simple-works/tcp-api-csharp 
    
    public sealed class TcpConnection : IDisposable
    {
        public bool Pulsable => !pulsing && !disposed && client.Connected;
        
        private TcpClient client;
        private bool pulsing;
        private bool disposed;
        private int byte_size;
        private byte[] byte_buffer;
        private StringBuilder stringBuilder;
        private ConcurrentQueue<string> rawMessageQueue;
        
        public TcpConnection(
            TcpClient tcpClient
        )
        {
            client          = tcpClient;
            pulsing         = false;
            disposed        = false;
            byte_size       = 4096;
            byte_buffer     = new byte[byte_size];
            stringBuilder   = new();
            rawMessageQueue = new();
        }
        
        public async Task Pulse(CancellationToken token)
        {
            pulsing    = true;
            var error  = false;
            var stream = client.GetStream();
            
            var cts        = CancellationTokenSource.CreateLinkedTokenSource(token);
            var timerToken = cts.Token;
            cts.CancelAfter(TimeSpan.FromSeconds(30)); // TODO: configure!
            try
            {
                var data = string.Empty;
                var i    = 0;
                
                stringBuilder.Clear();
                ClearBytes();
                // https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.networkstream.readasync?view=net-5.0 
                while ((i = await stream.ReadAsync(byte_buffer, 0, byte_buffer.Length, timerToken)) != 0)
                {
                    data = Encoding.UTF8.GetString(byte_buffer, 0, i);
                    stringBuilder.Append(data);
                    ClearBytes();
                    /*
                    string str = "Hey Device!";
                    Byte[] reply = System.Text.Encoding.ASCII.GetBytes(str);   
                    stream.Write(reply, 0, reply.Length);
                    */
                }
            }
            catch
            {
                error = true;
                throw;
            }
            finally
            {
                if (!error)
                {
                    var message = stringBuilder.ToString();
                    if (!string.IsNullOrWhiteSpace(message))
                    {
                        rawMessageQueue.Enqueue(message);
                    }
                }
                stream.Dispose();   
                pulsing = false;
            }
        }
        
        private void ClearBytes()
        {
            Array.Clear(byte_buffer, 0, byte_buffer.Length);
        }
        
        public void Dispose()
        {
            disposed = true;
            client.Close();
        }
    }
}
