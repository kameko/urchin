
namespace Caesura.Bots.Urchin.Core.Services.StandardServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Common.Configurations;
    
    // TODO: Abstract how we get memory
    // TODO: end program if memory is too high?
    
    public sealed class MemoryNotifierService : IService
    {
        public string Name => nameof(MemoryNotifierService);
        public bool Enabled { get; private set; }
        public bool Pulsable => true;
        public bool Started { get; private set; }
        
        private ILogger log;
        private int intervalInMinutes;
        private DateTimeOffset last_refresh;
        private long highest;
        
        public MemoryNotifierService(ILoggerFactory logger, UrchinConfigurationRoot config)
        {
            Enabled           = config.System.Services.MemoryNotifierService.Enabled;
            log               = logger.CreateLogger<MemoryNotifierService>();
            intervalInMinutes = 30;
            last_refresh      = DateTime.UtcNow;
            SetupFromConfig(config);
        }
        
        public Task Start(CancellationToken token)
        {
            Started = true;
            NotifiyMemoryUse();
            return Task.CompletedTask;
        }
        
        public Task Pulse(CancellationToken token)
        {
            if (last_refresh.AddMinutes(intervalInMinutes) < DateTime.UtcNow)
            {
                NotifiyMemoryUse();
                last_refresh = DateTime.UtcNow;
            }
            return Task.CompletedTask;
        }
        
        public ValueTask DisposeAsync() => new ValueTask();
        
        private void NotifiyMemoryUse()
        {
            var memory = GC.GetTotalMemory(false) / 1_048_576; // mebibytes
            if (memory > highest)
            {
                highest = memory;
            }
            // TODO: create a much more formatted message containing the date of the highest MB usage
            // and how many times it's been hit (if more than once)
            log.LogInformation("Current memory usage: {memory_mb} MB (Highest: {highest_mb})", memory, highest);
        }
        
        private void SetupFromConfig(UrchinConfigurationRoot config)
        {
            var interval = config.System.Services.MemoryNotifierService.IntervalInMinutes;
            if (interval > 0)
            {
                intervalInMinutes = interval;
            }
            else
            {
                log.LogWarning(
                    "MemoryNotifierService configuration value IntervalInMinutes is less than 0, " +
                    "resorting to default interval of {interval} minutes",
                    intervalInMinutes
                );
            }
        }
    }
}
