
namespace Caesura.AI.S2K3.Classic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.EntityFrameworkCore;
    using Bots.Urchin.Common.Infrastructure;
    using Bots.Urchin.Common.Algorithms;
    using Entities;
    
    // This is all a bit hacky, but I'm on a tight deadline.
    // I'll write a completely superior and cleaner version later.
    
    // TODO: if chain isn't full, don't refresh.
    // TODO: ignore messages of a certain length.
    // TODO: Some method for a user to say "the last message I sent is important", and Classic will
    // incorporate the most recent "important" messages in it's chain, sacrificing mundane messages.
    // This will involve passing the message ID to the AI. To compensate for this, the AI project
    // should have a general message type that contains all this data, sort of an extension of the
    // MarkovMessage type we have now.
    // TODO: check if it can post an emote before trying. emotes from guilds the bot isn't in
    // cannot display properly.
    
    // TODO: enhance markov chain by making a fork of it specifically to handle
    // strings/data using a fuzzy method. It will use the following:
    // - Levenshtein distance with randomness
    // - Remove casing and punctuation
    
    public sealed class MarkovResponder
    {
        private ILogger log;
        private MarkovConfig config;
        private DbContextOptions<MarkovMessageContext> db_options;
        private ExceptionListener listener;
        
        private ConcurrentDictionary<string, MarkovChain<MarkovChainItem>> chains;
        private ConcurrentQueue<WorkItem> work_queue;
        private CancellationTokenSource cts;
        private Thread workThread;
        private DateTimeOffset last_refresh;
        private bool new_message_received; // track and prevent reading from the database multiple times.
        
        private readonly object work_lock = new object();
        
        public MarkovResponder(
            ILoggerFactory logFactory,
            MarkovConfig configuration,
            DbContextOptions<MarkovMessageContext> dbOptions,
            ExceptionListener exceptionListener
        )
        {
            log        = logFactory.CreateLogger<MarkovResponder>();
            config     = configuration;
            db_options = dbOptions;
            listener   = exceptionListener;
            
            chains     = new();
            work_queue = new();
            cts        = new();
            workThread = new Thread(ThreadRuntime)
            {
                Name         = nameof(MarkovResponder),
                IsBackground = true,
            };
        }
        
        public async Task Start(CancellationToken token)
        {
            cts = CancellationTokenSource.CreateLinkedTokenSource(token);
            await ReadChainsFromDatabase();
            workThread.Start();
            log.LogInformation("Classic AI started");
        }
        
        public Task Shutdown()
        {
            cts.Cancel();
            return Task.CompletedTask;
        }
        
        public void ConfigUpdated(MarkovConfig newConfig)
        {
            lock (work_lock)
            {
                config = newConfig;
            }
        }
        
        public void NotifyMessage(MarkovMessage message)
        {
            // maybe todo
        }
        
        public async Task StoreMessage(MarkovMessage markovMessage)
        {
            using (var context = new MarkovMessageContext(db_options))
            {
                context.Add(markovMessage);
                await context.SaveChangesAsync();
            }
        }
        
        public Task PumpMessage(MarkovMessage message, Func<string, Task> onReady)
        {
            work_queue.Enqueue(new WorkItem(message, onReady));
            return Task.CompletedTask;
        }
        
        private async Task<(bool, string?)> HandleMessage(MarkovMessage markovMessage)
        {
            var source  = markovMessage.Source!;
            var sender  = markovMessage.Sender!;
            var message = markovMessage.Message!;
            
            await StoreMessage(markovMessage);
            
            if (!string.IsNullOrWhiteSpace(message))
            {
                new_message_received = true;
            }
            
            if (config.Sources is not null)
            {
                var this_config = config.Sources.ToList().Find(x => x.Source == source);
                if (this_config is not null)
                {
                    if (config.GroupSource is not null)
                    {
                        foreach (var group in config.GroupSource)
                        {
                            if (group.Name is not null &&
                                group.Sources is not null &&
                                (group.Sources.Contains(this_config.Source) ||
                                group.Sources.Contains(this_config.FriendlyName))
                            )
                            {
                                AddToChain(group.Name, sender, message);
                            }
                        }
                    }
                    else
                    {
                        AddToChain(source, sender, message);
                    }
                }
            }
            
            var can_respond = CanRespond(message.ToLower());
            if (can_respond)
            {
                lock (work_lock)
                {
                    if (!config.Sources?.Any(x => x.Source == source) ?? true)
                    {
                        log.LogWarning(
                            "Responder not registered to respond to source \"{source}\"",
                            source
                        );
                        return (false, null);
                    }
                    else
                    {
                        MarkovChain<MarkovChainItem>? chain = null!;
                        
                        if (config.Sources is not null && config.GroupSource is not null)
                        {
                            var this_config = config.Sources.ToList().Find(x => x.Source == source);
                            if (this_config is not null)
                            {
                                foreach (var group in config.GroupSource)
                                {
                                    if (group.Name is not null &&
                                        group.Sources is not null &&
                                        (group.Sources.Contains(this_config.Source) ||
                                        group.Sources.Contains(this_config.FriendlyName))
                                    )
                                    {
                                        if (!chains.ContainsKey(group.Name))
                                        {
                                            return (true, "[ERROR] Chain does not exist.");
                                        }
                                        chain = chains[group.Name];
                                    }
                                }
                            }
                        }
                        
                        if (chain is null)
                        {
                            if (!chains.ContainsKey(source))
                            {
                                return (true, "[ERROR] Chain does not exist.");
                            }
                            chain = chains[source];
                        }
                        
                        var response = "[ERROR] Could not generate response.";
                        try
                        {
                            if (config.RejectOutputsWithKeywords is not null)
                            {
                                var countdown = 10;
                                var success   = true;
                                var new_resp  = string.Empty;
                                while (countdown > 0)
                                {
                                    var response_words = chain.Walk().Take(config.MaxWordResponseCount);
                                    new_resp = string.Join(" ", response_words).Trim();
                                    
                                    foreach (var keyword in config.RejectOutputsWithKeywords)
                                    {
                                        if (new_resp.Contains(keyword))
                                        {
                                            success = false;
                                            break;
                                        }
                                    }
                                    
                                    if (success && !string.IsNullOrWhiteSpace(new_resp))
                                    {
                                        response = new_resp;
                                        break;
                                    }
                                    
                                    countdown--;
                                }
                            }
                            else
                            {
                                var response_words = chain.Walk().Take(config.MaxWordResponseCount);
                                var new_resp = string.Join(" ", response_words).Trim();
                                if (!string.IsNullOrWhiteSpace(new_resp))
                                {
                                    response = new_resp;
                                }
                            }
                        }
                        catch
                        {
                            // ignore.
                        }
                        return (true, response);
                    }
                }
            }
            
            return (false, null);
        }
        
        private bool CanRespond(string message)
        {
            if (config.ResponseKeywords is null)
            {
                return false;
            }
            
            var positive = false;
            
            lock (work_lock)
            {
                if (config.ResponseKeywords is not null)
                {
                    foreach (var keyword in config.ResponseKeywords)
                    {
                        if (message.Contains(keyword))
                        {
                            positive = true;
                            break;
                        }
                    }
                }
                
                if (config.ResponseKeywordBlacklist is not null)
                {
                    foreach (var keyword in config.ResponseKeywordBlacklist)
                    {
                        if (message.Contains(keyword))
                        {
                            return false;
                        }
                    }
                }
            }
            
            return positive;
        }
        
        private bool AddToChain(string source, string sender, string message)
        {
            lock (work_lock)
            {
                if (config.Sources is not null && config.GroupSource is not null)
                {
                    var this_config = config.Sources.ToList().Find(x => x.Source == source);
                    if (this_config is not null)
                    {
                        var success = false;
                        foreach (var group in config.GroupSource)
                        {
                            if (group.Name is not null &&
                                group.Sources is not null &&
                                (group.Sources.Contains(this_config.Source) ||
                                group.Sources.Contains(this_config.FriendlyName))
                            )
                            {
                                Add(group.Name);
                                success = true;
                            }
                        }
                        
                        if (success)
                        {
                            return true;
                        }
                    }
                }
                
                if (config.RejectOutputsWithKeywords is not null)
                {
                    foreach (var bad_keyword in config.RejectOutputsWithKeywords)
                    {
                        if (message.ToLower().Contains(bad_keyword.ToLower()))
                        {
                            return false;
                        }
                    }
                }
                if (config.ResponseKeywordBlacklist is not null)
                {
                    foreach (var bad_keyword in config.ResponseKeywordBlacklist)
                    {
                        if (message.ToLower().Contains(bad_keyword.ToLower()))
                        {
                            return false;
                        }
                    }
                }
                if (config.SenderBlacklist is not null)
                {
                    foreach (var bad_sender in config.SenderBlacklist)
                    {
                        if (sender == bad_sender)
                        {
                            return false;
                        }
                    }
                }
                if (config.Sources is not null)
                {
                    var this_config = config.Sources.ToList().Find(x => x.Source == source);
                    foreach (var other_source in config.Sources)
                    {
                        if (
                            other_source.Source is not null &&
                            source != other_source.Source &&
                            other_source.IncludeFromOtherSources is not null
                        )
                        {
                            if (other_source.IncludeFromOtherSources.Contains(source) ||
                                (this_config is not null &&
                                other_source.IncludeFromOtherSources.Contains(this_config.FriendlyName))
                            )
                            {
                                Add(other_source.Source);
                            }
                        }
                    }
                }
                
                return Add(source);
            }
            
            bool Add(string chain_source)
            {
                var success = chains.TryGetValue(chain_source, out var chain);
                if (!success)
                {
                    log.LogInformation(
                        "Creating Markov chain for source \"{source}\" with depth {depth}",
                        chain_source,
                        config.Depth
                    );
                    chain = new MarkovChain<MarkovChainItem>(config.Depth);
                    chain = chains.GetOrAdd(chain_source, chain);
                }
                
                if (chain!.Size < (ulong)config.MaxChainSize)
                {
                    // log.LogInformation("Loading Message into {source}: {message}", chain_source, message);
                    
                    var words = message.Replace("\n", " ").Split().Select(x => new MarkovChainItem(x));
                    chain.Add(words);
                    return true;
                }
                return false;
            }
        }
        
        private Task ReadChainsFromDatabase()
        {
            last_refresh = DateTime.UtcNow;
            
            lock(work_lock)
            {
                using (var context = new MarkovMessageContext(db_options))
                {
                    if (context.Messages is not null && context.Messages.Count() > 0)
                    {
                        var query = context.Messages.OrderByDescending(x => x);
                        var max   = config.MaxChainSize;
                        foreach (var message in query)
                        {
                            if (message.Source is null || message.Sender is null || message.Message is null)
                            {
                                continue;
                            }
                            if (max <= 0)
                            {
                                break;
                            }
                            
                            if (config.Sources is not null)
                            {
                                var this_config = config.Sources.ToList().Find(x => x.Source == message.Source);
                                if (this_config is not null)
                                {
                                    if (config.GroupSource is not null)
                                    {
                                        foreach (var group in config.GroupSource)
                                        {
                                            if (group.Name is not null &&
                                                group.Sources is not null &&
                                                (group.Sources.Contains(this_config.Source) ||
                                                group.Sources.Contains(this_config.FriendlyName))
                                            )
                                            {
                                                
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                continue;
                            }
                            
                            if (config.RejectOutputsWithKeywords is not null)
                            {
                                var continue1 = false;
                                foreach (var filter in config.RejectOutputsWithKeywords)
                                {
                                    if (message.Message.ToLower().Contains(filter.ToLower()))
                                    {
                                        continue1 = true;
                                        break;
                                    }
                                }
                                if (continue1)
                                {
                                    continue;
                                }
                            }
                            if (config.SenderBlacklist is not null)
                            {
                                var continue2 = false;
                                foreach (var bad_sender in config.SenderBlacklist)
                                {
                                    if (message.Sender == bad_sender)
                                    {
                                        continue2 = true;
                                        break;
                                    }
                                }
                                if (continue2)
                                {
                                    continue;
                                }
                            }
                            
                            AddToChain(message.Source, message.Sender, message.Message);
                            
                            max--;
                        }
                    }
                }
            }
            
            var memory = GC.GetTotalMemory(false) / 1_048_576; // mebibytes
            log.LogInformation("Done seeding chains. Current memory usage is {memory_mb} MB", memory);
            
            return Task.CompletedTask;
        }
        
        private void ThreadRuntime()
        {
            TaskRuntime().Wait();
        }
        
        private Task TaskRuntime()
        {
            return Task.Run(async () =>
            {
                await Runtime();
            })
            .ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    listener.Tell(task.Exception!);
                }
            });
        }
        
        private async Task Runtime()
        {
            var token = cts.Token;
            while (!token.IsCancellationRequested)
            {
                var success = work_queue.TryDequeue(out var work_item);
                if (success)
                {
                    var (can_respond, message) = await HandleMessage(work_item!.Message);
                    if (can_respond && !string.IsNullOrWhiteSpace(message))
                    {
                        try
                        {
                            await work_item.OnResponseReady(message);
                        }
                        catch (Exception e)
                        {
                            log.LogWarning(
                                "Exception encountered when attempting to respond. Ignoring." + Environment.NewLine +
                                " --> {exception_message}",
                                e.Message
                            );
                        }
                    }
                }
                else
                {
                    if (last_refresh.AddMinutes(config.RefreshIntervalMinutes) < DateTime.UtcNow && new_message_received)
                    {
                        log.LogInformation("Reseeding markov chains...");
                        
                        chains.Clear();
                        await ReadChainsFromDatabase();
                        new_message_received = false;
                    }
                    
                    await Task.Delay(16);
                }
            }
        }
        
        private class WorkItem
        {
            public MarkovMessage Message { get; set; }
            public Func<string, Task> OnResponseReady { get; set; }
            
            public WorkItem(MarkovMessage message, Func<string, Task> onReady)
            {
                Message         = message;
                OnResponseReady = onReady;
            }
        }
        
        private class MarkovChainItem : IEquatable<MarkovChainItem>
        {
            public string Value { get; private set; }
            private int lower_hash;
            
            public MarkovChainItem(string value)
            {
                Value = value;
                lower_hash = value.ToLower().GetHashCode();
            }
            
            public static implicit operator MarkovChainItem(string str) => new MarkovChainItem(str);
            
            public bool Equals(MarkovChainItem? item) => item is not null ? lower_hash == item.lower_hash : false;
            
            public override int GetHashCode() => lower_hash;
            
            public override bool Equals(object? other) => other is MarkovChainItem mci ? Equals(mci) : false;
            
            public override string ToString() => Value;
        }
    }
}
