
namespace Caesura.AI.S2K3.Classic.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    public sealed class MarkovConfig
    {
        public IEnumerable<MarkovGroupSourceConfig>? GroupSource { get; set; } = new List<MarkovGroupSourceConfig>() { new MarkovGroupSourceConfig() };
        public IEnumerable<MarkovSourceConfig>? Sources { get; set; } = new List<MarkovSourceConfig>() { new MarkovSourceConfig() };
        public IEnumerable<string>? RejectOutputsWithKeywords { get; set; } = new List<string>();
        public int Depth { get; set; } = 2;
        public int MaxChainSize { get; set; } = 10_000;
        public int RefreshIntervalMinutes { get; set; } = 30;
        public int MaxWordResponseCount { get; set; } = 35;
        public IEnumerable<string>? ResponseKeywords { get; set; } = new List<string>();
        public IEnumerable<string>? ResponseKeywordBlacklist { get; set; } = new List<string>();
        public IEnumerable<string>? SenderBlacklist { get; set; } = new List<string>();
    }
    
    public class MarkovSourceConfig
    {
        public string? FriendlyName { get; set; } = "Your Source Friendly Name Here";
        public string? Source { get; set; } = "0";
        public IEnumerable<string>? IncludeFromOtherSources { get; set; } = new List<string>();
    }
    
    public class MarkovGroupSourceConfig
    {
        public string? Name { get; set; } = "Name Here";
        public IEnumerable<string>? Sources { get; set; } = new List<string>();
    }
}
