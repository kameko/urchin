
namespace Caesura.AI.S2K3.Classic.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    
    public sealed class MarkovMessage
    {
        public int Id { get; set; }
        public DateTimeOffset? TimeStamp { get; set; }
        public string? Source { get; set; }
        public string? Sender { get; set; }
        public string? Message { get; set; }
    }
    
    public sealed class MarkovMessageContext : DbContext
    {
        public DbSet<MarkovMessage>? Messages { get; set; }
        
        public MarkovMessageContext(DbContextOptions<MarkovMessageContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
