
namespace Caesura.Bots.Urchin.Common.DataBuffers
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface IBufferOwnershipToken : IDisposable
    {
        void Release();
    }
}
