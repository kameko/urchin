
namespace Caesura.Bots.Urchin.Common.DataBuffers
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    // TODO: A setup phase and then a running phase. Buffers may only be taken out
    // during the setup phase, it is an error to take any buffers out at runtime.
    // Buffers are meant to be a part of the hard-coded logic of the program.
    // For instance, if you have a service that can create and destroy instances
    // of itself, then simply take out 10 or so buffers and make it an error
    // to start any more services than that, etc..
    
    public interface IBufferAuthoritySetup
    {
        
    }
    
    public interface IBufferAuthority
    {
        
    }
}
