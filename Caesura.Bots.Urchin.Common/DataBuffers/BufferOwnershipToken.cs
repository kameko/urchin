
namespace Caesura.Bots.Urchin.Common.DataBuffers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    // TODO:
    
    public sealed class BufferOwnershipToken : IBufferOwnershipToken
    {
        internal BufferOwnershipToken()
        {
            
        }
        
        public void Release()
        {
            
        }
        
        public void Dispose() => Release();
    }
}
