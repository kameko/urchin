
namespace Caesura.Bots.Urchin.Common.DataBuffers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    public sealed class BufferId
    {
        public string Name { get; private set; }
        public int Id { get; private set; }
        
        public BufferId(string name, int id)
        {
            Name = name;
            Id   = id;
        }
        
        public BufferId(int id) : this(string.Empty, id) { }
        
        public static bool operator == (BufferId? o1, BufferId? o2)
        {
            if (o1 is null && o2 is null)
            {
                return true;
            }
            else if (o1 is null || o2 is null)
            {
                return false;
            }
            
            return o1.Equals(o2);
        }
        
        public static bool operator != (BufferId o1, BufferId o2) => !(o1 == o2);
        
        public bool Equals(BufferId other) => object.ReferenceEquals(this, other) || Id == other.Id;
        public override bool Equals(object? obj) => obj is BufferId other ? Equals(other) : false;
        public override int GetHashCode() => Id.GetHashCode();
        public override string ToString() => 
            string.IsNullOrWhiteSpace(Name)
            ? Id.ToString()
            : $"{Name} ({Id})";
    }
}
