
namespace Caesura.Bots.Urchin.Common.DataBuffers
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface IDataBuffer
    {
        BufferId Id { get; }
        bool HasValue { get; }
        bool Locked { get; }
        
        BufferOwnershipToken Lock();
        bool TryLock(out BufferOwnershipToken? token);
    }
    
    public interface IReadBuffer : IDataBuffer
    {
        bool CanRead { get; }
    }
    
    public interface IReadBuffer<T> : IReadBuffer
    {
        event Func<T> OnValueChanged;
        event Action OnValueVoided;
        
        T Read(BufferOwnershipToken token);
        bool TryRead(out T? item, BufferOwnershipToken token);
    }
    
    public interface IWriteBuffer : IDataBuffer
    {
        bool CanWrite { get; }
        
        void VoidValue(BufferOwnershipToken token);
    }
    
    public interface IWriteBuffer<T> : IWriteBuffer
    {
        void Write(T item, BufferOwnershipToken token);
        bool TryWrite(T item, BufferOwnershipToken token);
    }
    
    public interface IReadWriteBuffer : IReadBuffer, IWriteBuffer
    {
        
    }
    
    public interface IReadWriteBuffer<T> : IReadWriteBuffer, IReadBuffer<T>, IWriteBuffer<T>
    {
        IReadBuffer<T> RequestReadAccess();
        IWriteBuffer<T> RequestWriteAccess();
    }
}
