
namespace Caesura.Bots.Urchin.Common.Configurations
{
    using System.Collections.Generic;
    
    public sealed class UrchinDiscordConfiguration : ICanBeValidated
    {
        public bool AutoStart { get; set; } = false;
        public bool AllIntents { get; set; } = false;
        public string Token { get; set; } = "YOUR_TOKEN_HERE";
        public string? Status { get; set; } = "Urchin v{$system_version}";
        public string CommandPrefix { get; set; } = ".\\";
        public List<UrchinDiscordCommandConiguration> Commands { get; set; } = new();
        public List<UrchinDiscordConfigurationFriendlyNameKeyValuePair> Admins { get; set; } = new();
        public UrchinDiscordServicesConfiguration Services { get; set; } = new();
        
        public static UrchinDiscordConfiguration GetEmittable()
        {
            return new UrchinDiscordConfiguration()
            {
                Commands = new()
                {
                    new("shutdown"),
                    new("echo"),
                    new("remoteecho"),
                    new("scan"),
                    new("setavatar"),
                    new("setactivity"),
                    new("help"),
                    new("source"),
                },
                Admins   = new() { new() },
                Services = UrchinDiscordServicesConfiguration.GetEmittable(),
            };
        }
        
        public bool IsSetup(out string message)
        {
            // Simplistic check to see if the token resembles a Discord token.
            // See "Information/Discord Token Dissect.png" for more information.
            var token_parts = Token.Split('.');
            if (token_parts.Length != 3)
            {
                message = "Invalid discord token, please set the token";
                return false;
            }
            
            message = string.Empty;
            return true;
        }
        
        public bool IsValid() =>
            Token         is not null &&
            // Status can be null
            CommandPrefix is not null &&
            Services      is not null &&
            Services.IsValid()        &&
            IsCommandsAndAdminsValid();
        
        private bool IsCommandsAndAdminsValid()
        {
            if (Commands is null || Admins is null)
            {
                return false;
            }
            
            foreach (var item in Commands)
            {
                if (!item.IsValid())
                {
                    return false;
                }
            }
            
            foreach (var item in Admins)
            {
                if (!item.IsValid())
                {
                    return false;
                }
            }
            
            return true;
        }
    }
    
    public sealed class UrchinDiscordConfigurationFriendlyNameKeyValuePair
    {
        public string FriendlyName { get; set; } = "Friendly Name Here";
        public ulong Id { get; set; } = 0;
        
        public void Deconstruct(out string name, out ulong id)
        {
            name = FriendlyName;
            id   = Id;
        }
        
        public bool IsValid() => FriendlyName is not null;
    }
    
    public sealed class UrchinDiscordCommandConiguration : ICanBeValidated
    {
        public bool Enabled { get; set; } = false;
        public string Name { get; set; } = "Command Name Here";
        public List<UrchinDiscordConfigurationFriendlyNameKeyValuePair> Authorized { get; set; } = new() { new() };
        public List<UrchinDiscordConfigurationFriendlyNameKeyValuePair> AuthorBlacklist { get; set; } = new() { new() };
        
        public UrchinDiscordCommandConiguration()
        {
            
        }
        
        public UrchinDiscordCommandConiguration(string name)
        {
            Name = name;
        }
        
        public bool IsValid() => Name is not null && IsAuthorizedAndAuthorBlacklistValid();
        
        private bool IsAuthorizedAndAuthorBlacklistValid()
        {
            if (Authorized is null || AuthorBlacklist is null)
            {
                return false;
            }
            
            foreach (var item in Authorized)
            {
                if (!item.IsValid())
                {
                    return false;
                }
            }
            
            foreach (var item in AuthorBlacklist)
            {
                if (!item.IsValid())
                {
                    return false;
                }
            }
            
            return true;
        }
    }
    
    public sealed class UrchinDiscordServicesConfiguration : ICanBeValidated
    {
        public UrchinDiscordAttachmentDownloaderConfiguratuion AttachmentDownloader { get; set; } = new();
        
        public static UrchinDiscordServicesConfiguration GetEmittable()
        {
            return new UrchinDiscordServicesConfiguration()
            {
                AttachmentDownloader = UrchinDiscordAttachmentDownloaderConfiguratuion.GetEmittable(),
            };
        }
        
        public bool IsValid() => AttachmentDownloader is not null && AttachmentDownloader.IsValid();
    }
    
    public sealed class UrchinDiscordAttachmentDownloaderConfiguratuion
    {
        public bool Enabled { get; set; }
        public string SavePath { get; set; } = "discord_attachments";
        public bool AutoDelete { get; set; } = true;
        public int DeleteOldestFolderAfterDays { get; set; } = 3;
        public int DeleteCheckHourInterval { get; set; } = 6;
        public bool ChannelsIsWhitelist { get; set; } = false;
        public List<UrchinDiscordConfigurationFriendlyNameKeyValuePair> Channels { get; set; } = new();
        public List<UrchinDiscordConfigurationFriendlyNameKeyValuePair> AuthorBlacklist { get; set; } = new();
        
        public static UrchinDiscordAttachmentDownloaderConfiguratuion GetEmittable()
        {
            return new UrchinDiscordAttachmentDownloaderConfiguratuion()
            {
                Channels        = new() { new() },
                AuthorBlacklist = new() { new() },
            };
        }
        
        public bool IsValid() =>
            SavePath            is not null &&
            DeleteOldestFolderAfterDays > 0 &&
            DeleteCheckHourInterval     > 0 &&
            IsChannelsAndAuthorBlacklistValid();
        
        private bool IsChannelsAndAuthorBlacklistValid()
        {
            if (Channels is null || AuthorBlacklist is null)
            {
                return false;
            }
            
            foreach (var item in Channels)
            {
                if (!item.IsValid())
                {
                    return false;
                }
            }
            
            foreach (var item in AuthorBlacklist)
            {
                if (!item.IsValid())
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}
