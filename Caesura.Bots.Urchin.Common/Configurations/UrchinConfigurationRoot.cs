
namespace Caesura.Bots.Urchin.Common.Configurations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Text.Json;
    using Microsoft.Extensions.Configuration;
    using Extensions;
    
    public sealed class UrchinConfigurationRoot : ICanBeValidated
    {
        public UrchinSystemConfiguration System { get; set; }
        public UrchinDiscordConfiguration Discord { get; set; }
        
        public UrchinConfigurationRoot()
        {
            System  = new();
            Discord = new();
        }
        
        public UrchinConfigurationRoot(
            UrchinSystemConfiguration system,
            UrchinDiscordConfiguration discord
        )
        {
            System  = system;
            Discord = discord;
        }
        
        public static UrchinConfigurationRoot GetEmittable()
        {
            return new UrchinConfigurationRoot()
            {
                Discord = UrchinDiscordConfiguration.GetEmittable(),
            };
        }
        
        public bool IsSetup(out string message) => Discord!.IsSetup(out message);
        
        public bool IsValid() =>
            System  is not null &&
            Discord is not null &&
            System.IsValid()    &&
            Discord.IsValid();
        
        public void Emit(string path)
        {
            var options = new JsonSerializerOptions() { WriteIndented = true };
            var json    = JsonSerializer.Serialize(this, options);
            
            // Format JSON with 4 spaces instead of 2.
            // https://github.com/dotnet/runtime/issues/1174
            
            File.WriteAllLines(path, json.Indent());
        }
        
        public static UrchinConfigurationRoot Read(IConfigurationRoot configRoot)
        {
            // https://stackoverflow.com/a/47832908/12860405 
            // I have no words.
            var system  = configRoot.GetSection("System").Get<UrchinSystemConfiguration>();
            var discord = configRoot.GetSection("Discord").Get<UrchinDiscordConfiguration>();
            
            var config = new UrchinConfigurationRoot(system, discord);
            
            return config;
        }
    }
}
