
namespace Caesura.Bots.Urchin.Common.Configurations
{
    public sealed class UrchinSystemConfiguration : ICanBeValidated
    {
        public bool PromptAfterShutdown { get; set; } = true;
        public string SerilogConfigPath { get; set; } = "%configdir%/serilog.json";
        public UrchinServicesConfiguration Services { get; set; } = new();
        
        public bool IsValid() =>
            SerilogConfigPath is not null &&
            Services          is not null &&
            Services.IsValid();
    }
    
    public sealed class UrchinServicesConfiguration : ICanBeValidated
    {
        public MemoryNotifierServiceConfiguration MemoryNotifierService { get; set; } = new();
        public TcpServiceConfiguration TcpService { get; set; } = new();
        public DiscordServiceContainerConfiguration DiscordServiceContainer { get; set; } = new();
        
        public bool IsValid() =>
            MemoryNotifierService   is not null &&
            TcpService              is not null &&
            DiscordServiceContainer is not null &&
            TcpService.IsValid();
    }
    
    public class MemoryNotifierServiceConfiguration
    {
        public bool Enabled { get; set; } = true;
        public int IntervalInMinutes { get; set; } = 30;
    }
    
    public class TcpServiceConfiguration
    {
        public bool Enabled { get; set; } = true;
        public string IPAddress { get; set; } = "localhost";
        public int Port { get; set; } = 20_001;
        
        public bool IsValid() => 
            IPAddress            is not null &&
            IPAddress.Contains(' ') == false &&
            Port > 0;
    }
    
    public class DiscordServiceContainerConfiguration
    {
        public bool Enabled { get; set; } = true;
    }
}
