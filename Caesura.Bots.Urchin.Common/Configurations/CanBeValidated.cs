
namespace Caesura.Bots.Urchin.Common.Configurations
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface ICanBeValidated
    {
        bool IsValid();
    }
}
