
namespace Caesura.Bots.Urchin.Common.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    public interface IService
    {
        string Name { get; }
        bool Pulsable { get; }
        
        Task Pulse(CancellationToken token);
        T GetComponent<T>() where T : IComponent;
        T GetComponent<T>(string instanceId) where T : IComponent;
    }
}
