
namespace Caesura.Bots.Urchin.Common.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    // TODO: timed behaviors.
    
    public abstract class AbstractService : IService
    {
        public string Name { get; private set; }
        public bool Pulsable { get; protected set; }
        
        protected ExceptionListener Listener { get; private set; }
        
        public AbstractService(string name, ExceptionListener exceptionListener)
        {
            Name = name;
            
            Listener = exceptionListener;
        }
        
        public virtual Task Pulse(CancellationToken token)
        {
            return Task.CompletedTask;
        }
        
        public abstract T GetComponent<T>() where T : IComponent;
        public abstract T GetComponent<T>(string instanceId) where T : IComponent;
    }
}
