
namespace Caesura.Bots.Urchin.Common.Infrastructure.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    
    // TODO: Do *not* implement this in Common, implement it in Core.
    // Also move all other todo's into the new service system
    // TODO: If a service throws, tell it to shut down and then disable it and report it
    // to the user. The user can choose to restart it or just shut the system down.
    
    public interface IServiceSystem
    {
        
    }
}
