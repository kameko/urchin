
namespace Caesura.Bots.Urchin.Common.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    public sealed class SystemInformation
    {
        private static SystemInformation _instance;
        public static SystemInformation Instance => _instance;
        
        public string Name => "Urchin";
        public Version Version => new(0, 1, 0, 5);
        
        static SystemInformation()
        {
            _instance = new();
        }
        
        public SystemInformation()
        {
            
        }
    }
}
