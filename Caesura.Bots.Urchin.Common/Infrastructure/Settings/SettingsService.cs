
namespace Caesura.Bots.Urchin.Common.Infrastructure.Settings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Text.Json;
    using Configurations;
    using Extensions;
    using Abstractions;
    using Abstractions.Implementations;
    
    // TODO: when config is updated, update the file, then the in-memory version,
    // then pass the in-memory version to CheckIfUpdated's queue. This way the
    // configs are merely a backup, not an actual database.
    // Unless, of course, it was due to a filesystem watcher event.
    // This is primarily done as a sanitization system, if the config is invalid then
    // the setting should be rejected.
    // Check this with the ICanBeValidated interface.
    
    // TODO: make this an actual service (specially hard-coded in the service system)
    // and allow services to simply hold a unique Settings<T> type to get updates.
    
    public sealed class SettingsService
    {
        private string base_config_path;
        private IFileIO file_io;
        private Dictionary<string, ConfigContainer> configs;
        
        public SettingsService(string baseConfigPath, IFileIO fileIO)
        {
            base_config_path = baseConfigPath;
            file_io          = fileIO;
            configs          = new();
        }
        
        public SettingsService(string baseConfigPath) : this(baseConfigPath, new FileIO()) { }
        
        public Task Update(CancellationToken token)
        {
            
            return Task.CompletedTask;
        }
        
        public DateTimeOffset LastUpdated(string configNamespace)
        {
            var success = configs.TryGetValue(configNamespace, out var rawCfg);
            if (!success)
            {
                throw new InvalidOperationException($"Configuration namespace \"{configNamespace}\" does not exist.");
            }
            return rawCfg!.Updated;
        }
        
        public T GetConfig<T>(string configNamespace)
        {
            var success = configs.TryGetValue(configNamespace, out var rawCfg);
            if (!success)
            {
                throw new InvalidOperationException($"Configuration namespace \"{configNamespace}\" does not exist.");
            }
            var cfgObj = JsonSerializer.Deserialize<T>(rawCfg!.Raw)!;
            return cfgObj;
        }
        
        // We use this instead of an event so that a service can re-check it's
        // config at it's own opportune times. Similar to checking for a
        // canceled CancellationTask.
        public bool CheckIfUpdated<T>(string configNamespace, DateTimeOffset lastCheck, out T? config)
        {
            var success = configs.TryGetValue(configNamespace, out var rawCfg);
            if (!success)
            {
                throw new InvalidOperationException($"Configuration namespace \"{configNamespace}\" does not exist.");
            }
            
            if (lastCheck > rawCfg!.Updated)
            {
                var cfgObj = JsonSerializer.Deserialize<T>(rawCfg.Raw);
                config     = cfgObj;
                return true;
            }
            
            config = default;
            return false;
        }
        
        private class ConfigContainer
        {
            public string Raw;
            public DateTimeOffset Updated;
            
            public ConfigContainer(string raw)
            {
                Raw     = raw;
                Updated = DateTime.UtcNow;
            }
        }
    }
}
