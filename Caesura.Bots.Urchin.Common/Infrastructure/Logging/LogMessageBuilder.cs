
namespace Caesura.Bots.Urchin.Common.Infrastructure.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    // TODO: Logging framework for endpoint events, so we can both reuse the Discord logging stuff and have
    // the same logic and formatting to handle other endpoints, and make it far less painful to add/remove
    // endpoints.
    // We'll do this with a log builder, like
    //   log
    //     .Condition("private", () => msg.Channel.IsPrivate)
    //     .With("User Added:")
    //     .NewItem("Username", msg.User.Name) // autogenerates tag "{username}", or can define one manually in overload
    //       .IfNot(private)
    //         .With("\"user_nick\"", async () => msg.User.GetNickname())
    //       .EndIf()
    //       .With("({user_id})", msg.User.Id)
    //     .NewItem("Emotes", "[{emote_#}]", msg.Emotes) // generates "emote_1", "emote_2" etc
    //     .Log(LogLevel.Information); // information level is implicit
    // This builds a LogItemBuilder which contains a StringBuilder and an array of objects.
    // Automatically appends the service name when they ask for the log factory.
    // The best part is that, because it's a builder, it can be duplicated and shared across different
    // events, adding pre-made parts that are generic enough.
    // Output looks like this:
    // [DATE INF] DISCORD: User Added:
    // Username ¥ Dude "guy" (9827492480353)
    // Emotes   ¥ [Thing1], [Thing2]
    
    public sealed class LogMessageBuilder
    {
        
    }
}
