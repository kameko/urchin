
namespace Caesura.Bots.Urchin.Common.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    public static class Asserts
    {
        public static void True(bool condition)
        {
            if (!condition)
            {
                throw new AssertionException("Condition is false.");
            }
        }
        
        public static void False(bool condition)
        {
            if (condition)
            {
                throw new AssertionException("Condition is true.");
            }
        }
        
        public static void AreEqual(object? a, object? b)
        {
            if (a is null || b is null && !(a is null && b is null))
            {
                throw new AssertionException("Values are not equal.");
            }
            if (!a!.Equals(b))
            {
                throw new AssertionException("Values are not equal.");
            }
        }
        
        public static void NotEqual(object? a, object? b)
        {
            if (a is null && b is null)
            {
                throw new AssertionException("Values are equal (null).");
            }
            if (!(a is null && b is null))
            {
                return;
            }
            if (a!.Equals(b))
            {
                throw new AssertionException("Values are equal.");
            }
        }
        
        public static void NotNull(object? value)
        {
            if (value is null)
            {
                throw new AssertionException("Value is null.");
            }
        }
        
        public static void Is<T>(object? value)
        {
            if (value is not T)
            {
                throw new AssertionException($"Value is not of type {typeof(T)}.");
            }
        }
        
        public class AssertionException : Exception
        {
            public AssertionException() : base() { }
            public AssertionException(string message) : base(message) { }
        }
    }
}
