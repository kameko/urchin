
namespace Caesura.Bots.Urchin.Common.Abstractions.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    
    public sealed class FileIO : IFileIO
    {
        public FileIO()
        {
            
        }
        
        public bool Exists(string path)
        {
            return File.Exists(path);
        }
        
        public Stream Open(string path, FileMode mode, FileAccess access)
        {
            return File.Open(path, mode, access);
        }
        
        public bool Delete(string path)
        {
            var exists = Exists(path);
            if (exists)
            {
                File.Delete(path);
            }
            return exists;
        }
    }
}
