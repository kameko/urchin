
namespace Caesura.Bots.Urchin.Common.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    
    public interface IFileIO
    {
        bool Exists(string path);
        Stream Open(string path, FileMode mode, FileAccess access);
        bool Delete(string path);
    }
}
