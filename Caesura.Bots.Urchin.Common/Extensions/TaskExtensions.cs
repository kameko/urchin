
namespace Caesura.Bots.Urchin.Common.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    
    public static class TaskExtensions
    {
        /// <summary>
        /// For tasks that cannot be canceled. Wait for the task to complete before the CancellationToken
        /// is canceled. If the operation is canceled, throw OperationCanceledException. This should ideally
        /// only be used during program shutdown when it doesn't matter what the result of the task is.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="cancellationToken"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<T> WithWaitCancellation<T>(this Task<T> task, CancellationToken cancellationToken) 
        {
            // https://stackoverflow.com/a/14524565/12860405
            // The task completion source. 
            var tcs = new TaskCompletionSource<bool>(); 

            // Register with the cancellation token.
            using(cancellationToken.Register(s => ((TaskCompletionSource<bool>)s!)!.TrySetResult(true), tcs) ) 
            {
                // If the task waited on is the cancellation token...
                if (task != await Task.WhenAny(task, tcs.Task))
                {
                    throw new OperationCanceledException(cancellationToken); 
                }
            }

            // Wait for one or the other to complete.
            return await task; 
        }
    }
}
