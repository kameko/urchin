
namespace Caesura.Bots.Urchin.Common.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Text.RegularExpressions;
    
    public static class StringExtensions
    {
        public static int GetStableHashCode(this string str)
        {
            // https://stackoverflow.com/a/36845864/12860405 
            unchecked
            {
                int hash1 = 5381;
                int hash2 = hash1;

                for(int i = 0; i < str.Length && str[i] != '\0'; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1 || str[i+1] == '\0')
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i+1];
                }

                return hash1 + (hash2*1566083941);
            }
        }
        
        // https://stackoverflow.com/a/22804025/12860405 
        // https://donsnotes.com/tech/charsets/ascii.html 
        public static string RemoveControlChar(this string str) =>
            string.IsNullOrWhiteSpace(str)
            ? string.Empty
            : Regex.Replace(str!, @"[\u0000-\u001F]", string.Empty);
        
        public static List<string> Indent(this string str, int multiply = 1)
        {
            var lines_old = str.Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.None);
            var lines_new = new List<string>();
            foreach (var line in lines_old)
            {
                var count    = line.TakeWhile(Char.IsWhiteSpace).Count();
                var tab      = new string(' ', count * multiply);
                var new_line = tab + line;
                lines_new.Add(new_line);
            }
            return lines_new;
        }
    }
}
