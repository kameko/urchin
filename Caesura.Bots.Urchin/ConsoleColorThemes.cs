
namespace Caesura.Bots.Urchin
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Extensions.Logging;
    using Serilog.Sinks.SystemConsole.Themes;
    
    public static class ConsoleColorThemes
    {
        public static SystemConsoleTheme Literate => new(new Dictionary<ConsoleThemeStyle, SystemConsoleThemeStyle>
            {
                [ ConsoleThemeStyle.Text             ] = Color(ConsoleColor.White),
                [ ConsoleThemeStyle.SecondaryText    ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.TertiaryText     ] = Color(ConsoleColor.DarkGray),
                [ ConsoleThemeStyle.Invalid          ] = Color(ConsoleColor.Yellow),
                [ ConsoleThemeStyle.Null             ] = Color(ConsoleColor.Blue),
                [ ConsoleThemeStyle.Name             ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.String           ] = Color(ConsoleColor.Cyan),
                [ ConsoleThemeStyle.Number           ] = Color(ConsoleColor.Magenta),
                [ ConsoleThemeStyle.Boolean          ] = Color(ConsoleColor.Blue),
                [ ConsoleThemeStyle.Scalar           ] = Color(ConsoleColor.Green),
                [ ConsoleThemeStyle.LevelVerbose     ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.LevelDebug       ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.LevelInformation ] = Color(ConsoleColor.White),
                [ ConsoleThemeStyle.LevelWarning     ] = Color(ConsoleColor.Yellow),
                [ ConsoleThemeStyle.LevelError       ] = Color(ConsoleColor.White, ConsoleColor.Red),
                [ ConsoleThemeStyle.LevelFatal       ] = Color(ConsoleColor.White, ConsoleColor.Red),
            });
        
        public static SystemConsoleTheme Caesura => new(new Dictionary<ConsoleThemeStyle, SystemConsoleThemeStyle>
            {
                [ ConsoleThemeStyle.Text             ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.SecondaryText    ] = Color(ConsoleColor.DarkGray),
                [ ConsoleThemeStyle.TertiaryText     ] = Color(ConsoleColor.DarkGray),
                [ ConsoleThemeStyle.Invalid          ] = Color(ConsoleColor.Yellow),
                [ ConsoleThemeStyle.Null             ] = Color(ConsoleColor.Gray, ConsoleColor.DarkGray),
                [ ConsoleThemeStyle.Name             ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.String           ] = Color(ConsoleColor.DarkGray),
                [ ConsoleThemeStyle.Number           ] = Color(ConsoleColor.Red),
                [ ConsoleThemeStyle.Boolean          ] = Color(ConsoleColor.Blue),
                [ ConsoleThemeStyle.Scalar           ] = Color(ConsoleColor.Magenta),
                [ ConsoleThemeStyle.LevelVerbose     ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.LevelDebug       ] = Color(ConsoleColor.Gray),
                [ ConsoleThemeStyle.LevelInformation ] = Color(ConsoleColor.White),
                [ ConsoleThemeStyle.LevelWarning     ] = Color(ConsoleColor.Yellow),
                [ ConsoleThemeStyle.LevelError       ] = Color(ConsoleColor.DarkRed),
                [ ConsoleThemeStyle.LevelFatal       ] = Color(ConsoleColor.DarkRed, ConsoleColor.Red),
            });
        
        private static SystemConsoleThemeStyle Color(ConsoleColor fg) =>
            new SystemConsoleThemeStyle { Foreground = fg };
        private static SystemConsoleThemeStyle Color(ConsoleColor fg, ConsoleColor bg) =>
            new SystemConsoleThemeStyle { Foreground = fg, Background = bg, };
        
        public static void Demo(ILogger log)
        {
            log.LogInformation("Information.");
            log.LogWarning("Warning!");
            log.LogError("Error!");
            log.LogCritical("Critical!");
            log.LogDebug("Debug.");
            log.LogTrace("Trace.");
            
            log.LogInformation(
                "Testing Types:"     + Environment.NewLine +
                "Number  : {number}" + Environment.NewLine +
                "String  : {string}" + Environment.NewLine +
                "Null    : {null}"   + Environment.NewLine +
                "Boolean : {bool}"   + Environment.NewLine +
                "Scalar  : {guid}",
                72386458973234,
                "Hello, world!",
                null,
                false,
                Guid.NewGuid()
            );
        }
    }
}
