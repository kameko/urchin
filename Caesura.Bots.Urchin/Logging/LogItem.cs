
namespace Caesura.Bots.Urchin.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Serilog.Events;
    
    public sealed class LogItem
    {
        public int Id { get; set; }
        public LogEventLevel Level { get; set; }
        public string? LevelName { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string? FormattedMessage { get; set; }
        public string? MessageTemplate { get; set; }
        public IEnumerable<LogItemValueProperty>? ValueProperties { get; set; }
        public IEnumerable<LogItemSequenceProperty>? SequenceProperties { get; set; }
        public IEnumerable<LogItemDictionaryProperty>? DictionaryProperties { get; set; }
        public string? Exception { get; set; }
    }
    
    public sealed class LogItemValueProperty
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Value { get; set; }
        public string? Type { get; set; }
    }
    
    public sealed class LogItemSequenceProperty
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public IEnumerable<LogItemValueProperty>? Values { get; set; }
    }
    
    public sealed class LogItemDictionaryProperty
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public IEnumerable<LogItemDictionaryPropertyKeyValuePair>? Values { get; set; }
    }
    
    public sealed class LogItemDictionaryPropertyKeyValuePair
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Value { get; set; }
        public string? Type { get; set; }
    }
    
    public sealed class LogItemContext : DbContext
    {
        public DbSet<LogItem>? Items { get; set; }
        
        public LogItemContext(DbContextOptions<LogItemContext> options) : this(options, false)
        {
            
        }
        
        public LogItemContext(DbContextOptions<LogItemContext> options, bool ensureCreate) : base(options)
        {
            if (ensureCreate)
            {
                Database.EnsureCreated();
            }
        }
    }
}
