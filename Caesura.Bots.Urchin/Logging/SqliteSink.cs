
namespace Caesura.Bots.Urchin.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Text.Json;
    using Microsoft.EntityFrameworkCore;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;
    using Serilog.Configuration;
    
    // https://github.com/serilog/serilog/wiki/Developing-a-sink
    // https://github.com/serilog/serilog/tree/dev/src/Serilog/Events
    // https://github.com/serilog/serilog-settings-configuration
    
    // This is a bit hacky because I conceived of it in one painfully long night.
    // The formatting code needs work but otherwise I don't think this is so bad.
    
    public static class SqliteSinkExtensions
    {
        public static LoggerConfiguration Sqlite(
                this LoggerSinkConfiguration loggerConfiguration,
                string path,
                bool isAsync = true,
                IFormatProvider? formatProvider = null
        )
        {
            return loggerConfiguration.Sink(new SqliteSink(path, isAsync, formatProvider));
        }
    }
    
    public sealed class SqliteSink : ILogEventSink, IDisposable
    {
        private readonly IFormatProvider? format_provider;
        private DbContextOptions<LogItemContext> db_options;
        
        private string dbPath;
        private bool isRuntimeAsync;
        private ConcurrentQueue<Exception>? exceptions;
        private ConcurrentQueue<LogEvent>? workQueue;
        private CancellationTokenSource? cts;
        private Task? runtimeTask;

        public SqliteSink(string path, bool isAsync, IFormatProvider? formatProvider)
        {
            format_provider = formatProvider;
            isRuntimeAsync  = isAsync;
            dbPath          = Path.GetFullPath(path);
            db_options      = new DbContextOptionsBuilder<LogItemContext>()
                                      .UseSqlite($"Data Source={dbPath}")
                                      .Options;
            
            using (var context = new LogItemContext(db_options, ensureCreate: true))
            {
                context.SaveChanges();
            }
            
            if (isAsync)
            {
                exceptions  = new();
                workQueue   = new();
                cts         = new();
                runtimeTask = Task.Run(AsyncRuntime);
            }
        }

        public void Emit(LogEvent logEvent)
        {
            if (isRuntimeAsync)
            {
                workQueue!.Enqueue(logEvent);
                
                var success = exceptions!.TryDequeue(out var e);
                if (success)
                {
                    throw e!;
                }
            }
            else
            {
                Process(logEvent);
            }
        }
        
        public void Dispose()
        {
            cts?.Cancel();
            runtimeTask?.GetAwaiter().GetResult();
        }
        
        private async Task AsyncRuntime()
        {
            var token = cts!.Token;
            while (!token.IsCancellationRequested)
            {
                var success = workQueue!.TryDequeue(out var item);
                if (success)
                {
                    try
                    {
                        await ProcessAsync(item!);
                    }
                    catch (Exception e)
                    {
                        exceptions!.Enqueue(e);
                    }
                }
                else
                {
                    await Task.Delay(30);
                }
            }
        }
        
        private void Process(LogEvent logEvent)
        {
            using var context = new LogItemContext(db_options);
            
            var item = CreateLogItem(logEvent);
            
            context.Add(item);
            context.SaveChanges();
        }
        
        private async Task ProcessAsync(LogEvent logEvent)
        {
            using var context = new LogItemContext(db_options);
            
            var item = CreateLogItem(logEvent);
            
            await context.AddAsync(item);
            await context.SaveChangesAsync();
        }
        
        private LogItem CreateLogItem(LogEvent logEvent)
        {
            var message   = logEvent.RenderMessage(format_provider);
            var valprops  = new List<LogItemValueProperty>();
            var seqprops  = new List<LogItemSequenceProperty>();
            var dictprops = new List<LogItemDictionaryProperty>();
            
            foreach (var (name, prop) in logEvent.Properties)
            {
                if (prop is SequenceValue seq)
                {
                    FormatSequenceProperty(logEvent, valprops, seqprops, dictprops, name, seq);
                }
                else if (prop is DictionaryValue dict)
                {
                    FormatDictionaryProperty(logEvent, valprops, seqprops, dictprops, name, dict);
                }
                else
                {
                    FormatValueProperty(valprops, name, prop);
                }
            }
            
            var item = new LogItem();
            
            item.Timestamp            = logEvent.Timestamp;
            item.Level                = logEvent.Level;
            item.LevelName            = logEvent.Level.ToString();
            item.FormattedMessage     = message;
            item.MessageTemplate      = logEvent.MessageTemplate.Text;
            item.ValueProperties      = valprops;
            item.SequenceProperties   = seqprops;
            item.DictionaryProperties = dictprops;
            item.Exception            = logEvent.Exception?.ToString();
            
            return item;
        }
        
        private void FormatSequenceProperty(
            LogEvent logEvent,
            List<LogItemValueProperty> valprops,
            List<LogItemSequenceProperty> seqprops,
            List<LogItemDictionaryProperty> dictprops,
            string prop_name,
            SequenceValue seq_value
        )
        {
            var seqpropvals = new List<LogItemValueProperty>();
            
            var element_index = 0;
            foreach (var value in seq_value.Elements)
            {
                if (value is SequenceValue inner_seq)
                {
                    FormatSequenceProperty(logEvent, valprops, seqprops, dictprops, element_index.ToString(), inner_seq);
                }
                else if (value is DictionaryValue inner_dict)
                {
                    FormatDictionaryProperty(logEvent, valprops, seqprops, dictprops, element_index.ToString(), inner_dict);
                }
                else
                {
                    FormatValueProperty(seqpropvals, element_index.ToString(), value);
                }
                element_index++;
            }
            
            var seqprop = new LogItemSequenceProperty()
            {
                Name   = prop_name,
                Values = seqpropvals,
            };
            
            seqprops.Add(seqprop);
        }
        
        private void FormatDictionaryProperty(
            LogEvent logEvent,
            List<LogItemValueProperty> valprops,
            List<LogItemSequenceProperty> seqprops,
            List<LogItemDictionaryProperty> dictprops,
            string prop_name,
            DictionaryValue dict_value
        )
        {
            var dictpropvals = new List<LogItemDictionaryPropertyKeyValuePair>();
            
            foreach (var (key, value) in dict_value.Elements)
            {
                var nk = key.Value?.ToString() ?? "NULL";
                if (value is DictionaryValue inner_dict)
                {
                    FormatDictionaryProperty(logEvent, valprops, seqprops, dictprops, nk, inner_dict);
                }
                else if (value is SequenceValue seq)
                {
                    FormatSequenceProperty(logEvent, valprops, seqprops, dictprops, nk, seq);
                }
                else
                {
                    var dictpropkvp = new LogItemDictionaryPropertyKeyValuePair()
                    {
                        Name  = nk,
                        Value = value.ToString(),
                        Type  = value is ScalarValue sv ? sv.Value.GetType().FullName : null,
                    };
                    dictpropvals.Add(dictpropkvp);
                }
            }
            
            var dictprop = new LogItemDictionaryProperty()
            {
                Name   = prop_name,
                Values = dictpropvals,
            };
            
            dictprops.Add(dictprop);
        }
        
        private void FormatValueProperty(
            List<LogItemValueProperty> valprops,
            string prop_name,
            LogEventPropertyValue prop_value
        )
        {
            var valprop = new LogItemValueProperty();
            valprop.Name  = prop_name;
            valprop.Value = prop_value?.ToString();
            valprop.Type  = prop_value is ScalarValue sv ? sv.Value?.GetType().FullName : null;
            valprops.Add(valprop);
        }
    }
}
