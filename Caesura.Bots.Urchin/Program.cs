﻿
namespace Caesura.Bots.Urchin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Threading;
    using System.Runtime.InteropServices;
    using System.IO;
    using System.Text.Json;
    using System.Reflection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Configuration;
    using Microsoft.EntityFrameworkCore;
    using Serilog;
    using Common.Configurations;
    using Common.Infrastructure;
    using AI.S2K3.Classic;
    using AI.S2K3.Classic.Entities;
    using Core;
    using Core.Endpoints.UrchinDiscord;
    using Core.Services.StandardServices;
    using Core.Services.UrchinTcp;
    
    // Other endpoints to consider:
    // (vaguely sorted in priority from greatest to least, really just a suggestion.)
    // Matrix   : https://github.com/VRocker/MatrixAPI (need to fork it, it's ancient/unmaintained.)
    // Telegram : https://github.com/wiz0u/WTelegramClient / https://github.com/sochix/TLSharp 
    // Twitter  : https://github.com/CoreTweet/CoreTweet / https://github.com/JoeMayo/LinqToTwitter 
    //            (as much as I *loathe* to do it. too many people use it to ignore.)
    //            (scratch that, they just banned a friend for no good reason. Twitter is *strictly* prohibited.)
    // Steam    : https://github.com/SteamRE/SteamKit 
    // Guilded  : (whenever the API for it actually comes out.)
    // Fosscord : https://github.com/fosscord/FSharpPlus (since it's virtually free to implement, just need some refactoring)
    // MangaDex : (need to implement this ourselves.)
    //            Send all requests through Tor: https://github.com/joelverhagen/TorSharp 
    // YouTube  : (can't find good libraries for it, may just need to call out to youtube-dl.exe)
    
    // Todo's before shipping v1.0
    // TODO: make an install script that performs the following:
    //   invoke it with "build {dest}" or "build"
    //   if no destination folder defined in the argument, check a called "build.cfg"
    //   run "dotnet build -c Release"
    //   copy .build/Caesura.Bots.Urchin/bin/Release to {dest}/bin
    //   if {dest}/serilog.json does not exist, copy it from Information/serilog.json
    //   if {dest}/appsettings.json exists, print a warning about ensuring the config is up-to-date.
    // Make this a proper C# program and project, put the executable in the root folder. Check for
    // the .NET SDK, if it isn't installed, tell the user and exit (do not install it, thats too intrusive)
    // TODO: Implement a general-purpose AI wrapper and wrap Classic in it.
    // Also, move it out from the AI.S2k3 project entirely, and put it in a new AM (Autonomy Module) project.
    // AMs will just be normal in-process services, not stand-alone processes. They will be turned on and off
    // like any other service.
    // TODO: Use the Serilog Async sink for all sinks, including the custom Sqlite one:
    // https://github.com/serilog/serilog-sinks-async 
    // Exclude the console one, since it's quick and it gets slowed down by the sqlite logger.
    // NOTE: Do this *after* using Serilog.Expressions, to ensure that they don't conflict.
    // If they do, prioritize Serilog.Expressions over the Async sink, since things seem to
    // work just fine as they are now.
    // TODO: embed the default Serilog config, emit it to the target location if it doesn't exist.
    // TODO: filter console logs using this:
    // https://github.com/serilog/serilog-expressions 
    // https://stackoverflow.com/questions/46516359/filter-serilog-logs-to-different-sinks-depending-on-context-source
    // (maybe file logs too. but not database logs.)
    // use it to filter out bad discord channels.
    // Look for SourceContext "Caesura.Bots.Urchin.Core.Endpoints.UrchinDiscord.UrchinDiscordClient"
    // and match "channel_id" with the bad channels.
    // TODO: Do not use the Endpoint classes directly, have them held by a specialized system service
    // for each of them. And allow the services to use multiple instances of one Endpoint, so we can
    // have multiple Discord connections for multiple AIs. Rename/reuse the UrchinDiscordContainer for this.
    // Additionally, remove the endpoint interface entirely, since they're individually owned by services.
    // TODO: dice, conch and yes/no commands.
    // TODO: "info" command with alias "urchin".
    // TODO: define all the help docs for the commands.
    // TODO: Discord avatar rotator service.
    // TODO: A "leave guild" command.
    // TODO: Implement all discord commands as general-purpose services, the discord commands simply wrap
    // around them. This way we can use dice, avatar rotator etc. in other endpoints.
    // Also use the Common.Abstractions types to do this, so we no longer have to test commands outside
    // of testing the actual endpoint.
    // TODO: Also implelement a general-purpose command framework.
    // Our goal is to put the least amount of reusable code in Discord as possible since they plan to
    // kill bots eventually. The technologies we put into the Discord endpoint should be just as usable
    // to future endpoints for better chat clients.
    // Essentially, what an endpoint will do is say "here, command parser, is this a command? if not, give
    // me a suggestion, otherwise, tell me what command to run from the ones I gave you", then the endpoint-
    // specific command handler will run, which in turn will simply call more general-purpose services to
    // handle processing, and the endpoint-specific command handlers are only there to abstract the endpoint.
    // TODO: when making the new database config system, allow enabling/disabling guilds entirely so they
    // can be configured online without messing with anything.
    // Actually make the system use raw JSON files. This will work by being managed by a central service
    // that buffers reads/writes, uses file watchers for updates, and gracefully handles I/O errors with
    // retrys and falling back to default configs stored in-memory.
    // Put this in Common.Infrastructure.
    // TODO: A memory probing command and (or combined with) a GC collect command. From TCP, not Discord.
    // TODO: Implement proper DI and tests for all code interacting with I/O. Especially the Attachment
    // Downloader service.
    // TODO: A central service behavior inhibiting service. A service will tell the inhibitor "I plan to do
    // this, is it okay?" if it is, the inhibitor checks if anything else needs to be paused when it asks
    // the same question, and if so, tells it to wait. If a behavior to inhibit is still running, then
    // the asking service is told to wait instead, then the inhibitor will tell the second service to wait
    // next time and allow the first service to fulfill it's request.
    // This is useful for services such as the markov chain reporting memory use at the same time as the memory tracker.
    // This may involve refactoring service to give them more than a simple Pulse method.
    // While we're at it, give the base service class a state machine of some kind. This will also give the
    // inhibitor more information on what to do for services that need to perform one operation in multiple steps.
    // And remove the Discord service container, just put discord services as normal services, or part of the
    // discord service under the new service model. I suppose Discord services will now be Components.
    // All services expose both a public, DI-minded interface to access their services, a private interface
    // for orchistrating the service from the service manager/inhibitor.
    // Put the IService, BaseService, IComponent and BaseComponent types in the Common project, so anything
    // can use them. This will be extremely important for the AIs.
    // Also implement buffers, and use them for inter-service communication. Inter-service communication
    // is done through DI and is very explicit with type safety, there is no runtime checking.
    // TODO: For dealing with endpoint abstraction, we'll have two concepts, a data source (eg a message)
    // and a source supplier (website, discord channel, twitter account, etc.) Suppliers will be tagged
    // ("belongs to this guild" etc.) Data is tagged with various metadata, like creation time. Sources
    // are tagged with various data, like discovery time, nicknames (past and present), etc.
    // This will not only help abstract services but especially the AI itself.
    // TODO: Timer service which (with the help of other services/the service framework itself) tracks how
    // long all services take to execute. Possibly even have it intelligently reorganize longer-running
    // services into their own task, or put short-running services in the same cooperative update loop?
    // TODO: Logging framework for endpoint events, so we can both reuse the Discord logging stuff and have
    // the same logic and formatting to handle other endpoints, and make it far less painful to add/remove
    // endpoints.
    // TODO: Split out the Discord endpoint into it's own project, to be consistent with future projects
    // that will have to be their own projects (if forking or writing a custom implementation).
    // This todo, the one above, and the one about the global command processor essentially mean rewriting
    // the discord endpoint entirely.
    // TODO: Refactor Program.cs.
    // TODO: And please stop adding new features, this is some scary feature creep.
    
    // Plans for v2.0
    // TODO- Dictionary service and discord command to access it. Allows for adding custom definitions.
    // TODO- Discord role management commands.
    // TODO- In Common.Abstractions, abstract HttpClient and give an option to use it through Tor.
    //       Use TorSharp for the implemenetation in Common.Abstractions.Implementations
    //       https://github.com/joelverhagen/TorSharp 
    
    // Plans for C# 10/.NET 6
    // TODO: Reformat all code to use new top-level namespace and global using. Change all project files
    // from `<LangVersion>preview</LangVersion>` to `<LangVersion>10</LangVersion>`.
    // Only put extremely common things in the new global using file. System, Collections.Generic, Collections.
    // Concurrent, Threading, Tasks, MS Logging, and of course Urchin.Common(.Infrastructrure).
    // Do not put things like MS Config, EF, IO, etc. These things are more specific and are useful to tell
    // you what the source file is actually doing. This includes Common.Abstractions.
    // And define `<ImplicitUsings>disable</ImplicitUsings>`.
    // https://endjin.com/blog/2021/09/dotnet-csharp-10-implicit-global-using-directives.html 
    // Current list:
    // global using global::System;
    // global using global::System.Linq;
    // global using global::System.Collections.Generic;
    // global using global::System.Collections.Concurrent;
    // global using global::System.Threading;
    // global using global::System.Threading.Tasks;
    // global using global::Microsoft.Extensions.Logging;
    // global using global::Caesura.Bots.Urchin.Common.Extensions;
    // global using global::Caesura.Bots.Urchin.Common.Infrastructure;
    // global using global::Caesura.Bots.Urchin.Common.Infrastructure.Logging;
    
    // Plans for .NET 7
    // TODO- Make the remote client service switch between TCP or QUIC using System.Net.Quic depending on
    // if it's running on Windows 11 or not.
    
    public class Program
    {
        public bool PromptAfterShutdown { get; set; }
        public string[] Arguments { get; set; }
        
        public UrchinConfigurationRoot Config { get; set; }
        public ILoggerFactory LogFactory { get; set; }
        
        public ILogger<Program> Log { get; set; }
        public CancellationTokenSource Cts { get; set; }
        public ExceptionListener Listener { get; set; }
        
        public UrchinSystem UrchinSystem { get; set; }
        
        private int emergency_cancel_countdown = 3;
        private bool config_is_setup = false;
        private string config_not_setup_message = string.Empty;
        
        public Program(string[] args)
        {
            PromptAfterShutdown = false;
            Arguments           = args;
            
            Config     = GetConfig();
            LogFactory = GetLogFactory();
            
            Log      = LogFactory.CreateLogger<Program>();
            Cts      = new CancellationTokenSource();
            Listener = new ExceptionListener();
            
            UrchinSystem = new UrchinSystem(LogFactory, Config, Listener, Cts);
            
            // NOTE: When creating a new service, make sure to add it's configuration in
            // UrchinConfiguration/UrchinSystemConfiguration.cs
            UrchinSystem.Services.Add(new MemoryNotifierService(LogFactory, Config));
            UrchinSystem.Services.Add(new UrchinTcpService(LogFactory, Config, Listener));
            
            var responder = CreateMarkovResponder();
            
            var discord = new UrchinDiscordClient(LogFactory, Config, Listener, UrchinSystem.Services, responder);
            
            UrchinSystem.AddClient(discord);
        }
        
        public static async Task Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine("Caesura Software Solutions (R) Urchin Bot Framework version " + SystemInformation.Instance.Version.ToString());
            Console.WriteLine("Copyright (C) Caesura Software Solutions. All rights reserved.");
            Console.WriteLine();
            
            var program = new Program(args);
            await program.Start();
        }
        
        public async Task Start()
        {
            try
            {
                Setup();
                
                Log.LogInformation("Caesura Urchin v{version}", SystemInformation.Instance.Version);
                
                Log.LogInformation(
                    "Running on {framework_desc} {framework_arch} on {os_desc} {os_arch}",
                    RuntimeInformation.FrameworkDescription,
                    RuntimeInformation.ProcessArchitecture,
                    RuntimeInformation.OSDescription,
                    RuntimeInformation.OSArchitecture
                );
                
                await UrchinSystem.Start();
                await Listener.WaitAndThrowAsync(Cts.Token);
            }
            catch (Exception e)
            {
                Log.LogCritical(e, string.Empty);
                Cts.Cancel();
            }
            finally
            {
                await UrchinSystem.DisposeAsync();
                
                Log.LogCritical("Shutting down...");
                LogFactory.Dispose();
                
                if (PromptAfterShutdown)
                {
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
            }
        }
        
        private void Setup()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                Console.Title = "Urchin Server v" + SystemInformation.Instance.Version.ToString();
            }
            
            Cts.Token.Register(() => Log.LogDebug("System CancellationTokenSource cancelled."));
            
            SetupCancelKeyPress();
        }
        
        private ILoggerFactory GetLogFactory()
        {
            // TODO: When emitting the default Serilog config file, replace
            // %configdir% with GetDefaultDataPath
            var path           = Path.GetFullPath(
                Config.System.SerilogConfigPath.Replace(
                    "%configdir%",
                    GetDefaultDataPath(),
                    StringComparison.InvariantCultureIgnoreCase
                )
            );
            var exists         = File.Exists(path);
            var serilog_config = new LoggerConfiguration();
            if (exists)
            {
                var config_root = new ConfigurationBuilder()
                                    .AddJsonFile(path: path, optional: true)
                                    .Build();
                serilog_config = serilog_config.ReadFrom.Configuration(config_root);
            }
            else
            {
                var logConTem  = "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {NewLine}{Exception}";
                serilog_config = serilog_config
                                    .WriteTo.Console(
                                        outputTemplate: logConTem,
                                        theme: ConsoleColorThemes.Caesura
                                    );
            }
            
            var serilog = serilog_config.CreateLogger();
            var logger  = LoggerFactory.Create(builder =>
            {
                builder.AddSerilog(serilog, true);
            });
            
            Serilog.Debugging.SelfLog.Enable(msg => Console.WriteLine("SERILOG MESSAGE: " + msg));
            
            if (!exists)
            {
                serilog.Warning(
                    "Serilog configuration file does not exist, resorting to defaults." + Environment.NewLine +
                    " --> Path searched: {path}",
                    path
                );
            }
            if (!config_is_setup)
            {
                serilog.Warning(
                    "Configuration is not set up properly. Shut down the application, amend " +
                    "the issue and restart the application. Reason: {reason}",
                    config_not_setup_message
                );
            }
            
            return logger;
        }
        
        private UrchinConfigurationRoot GetConfig()
        {
            var config_path = Arguments.FirstOrDefault() ?? GetDefaultConfigPath();
            
            if (!File.Exists(config_path))
            {
                var temp_config = UrchinConfigurationRoot.GetEmittable();
                temp_config.Emit(config_path);
            }
            
            var config_root = new ConfigurationBuilder()
                                .AddJsonFile(path: config_path, optional: true)
                                .Build();
            
            var urchin_config = UrchinConfigurationRoot.Read(config_root);
            if (!urchin_config.IsValid())
            {
                throw new InvalidOperationException(
                    "Configuration file is not valid. Undo any changes you've made, or delete " +
                    "the file and let the system regenerate a fresh valid configuration."
                );
            }
            
            config_is_setup = urchin_config.IsSetup(out config_not_setup_message);
            
            return urchin_config;
        }
        
        /*
        private void PrintLoggerDemo()
        {
            Log.LogInformation(
                "Log Test {a}, {b}, {c}",
                4822,
                new Dictionary<int, int>() { { 44, 55 } },
                new List<List<int>>(){ new List<int>(){ 70, 80, 90 }, new List<int>(){} }
            );
            ConsoleColorThemes.Demo(Log);
        }
        */
        
        private void SetupCancelKeyPress()
        {
            Console.CancelKeyPress += (_, args) =>
            {
                Log.LogCritical("Initiating shutdown routine due to key combination {keys}.", args.SpecialKey);
                
                PromptAfterShutdown = false;
                args.Cancel         = true;
                
                Cts.Cancel();
                
                emergency_cancel_countdown--;
                if (emergency_cancel_countdown <= 0)
                {
                    Console.WriteLine("PROGRAM NOT SHUTTING DOWN PROPERLY, USING EMERGENCY SHUTDOWN.");
                    Environment.Exit(-1);
                }
            };
            
            if (Config.System.PromptAfterShutdown)
            {
                PromptAfterShutdown = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
            }
        }
        
        private MarkovResponder CreateMarkovResponder()
        {
            var path        = Path.Combine(GetDefaultDataPath(), "classic");
            var config_path = Path.Combine(path, "config.json");
            var db_path     = Path.Combine(path, "messages.db");
            
            var mkconfig = new MarkovConfig();
            
            Directory.CreateDirectory(path);
            if (!File.Exists(config_path))
            {
                var config_json = JsonSerializer.Serialize(
                    new MarkovConfig(),
                    new JsonSerializerOptions() { WriteIndented = true }
                );
                
                using (var stream = File.CreateText(config_path))
                {
                    stream.Write(config_json);
                }
            }
            else
            {
                var json = File.ReadAllText(config_path);
                mkconfig = JsonSerializer.Deserialize<MarkovConfig>(json);
            }
            
            var db_options  = new DbContextOptionsBuilder<MarkovMessageContext>()
                                        .UseSqlite($"Data Source={db_path}")
                                        .Options;
            
            var responder = new MarkovResponder(LogFactory, mkconfig!, db_options, Listener);
            
            UrchinSystem.OnStart    += responder.Start;
            UrchinSystem.OnShutdown += responder.Shutdown;
            
            return responder;
        }
        
        private static string GetDefaultConfigPath()
        {
            return Path.Combine(GetDefaultDataPath(), "appsettings.json");
        }
        
        private static string GetDefaultDataPath()
        {
            var data_in_superdir = false;
#if DATA_FOLDER_IN_SUPERDIR
            data_in_superdir = true;
#endif
            if (data_in_superdir)
            {
                return Directory.GetParent(
                    Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!
                )!.FullName;
            }
            else
            {
                return Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location)!);
            }
        }
    }
}
